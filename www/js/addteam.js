/****Add Team page methods(atpm)****/
console.warn("ATPM loaded");

var ATPM = {
    /**Add Team page literal**/
    fetchedMembersArray: [], // Array to hold the list of existing members
    newMemberArray: [], // Array to hold the list of New Members,
    selectedMembersArray: [], // Array to hold the selected members id
    teamLeaders: [], // Array to hold the team leader id
    selectedMemberHtml: '', // html to show selected members
    membersArray: [], // array to store details of members for a team
    validateAddMemberForm: function () {
        // Method to validate New member form
        if ($("#nmfullname").val() == "") {
            // If New member name is empty
            CM.showAlert(ErrorText.new_member_name_blank);
            $("#nmfullname").focus();
        } else if ($("#nmorganisationname").val() == "") {
            // If New member organisation is empty
            CM.showAlert(ErrorText.new_member_organisation_blank);
            $("#nmorganisationname").focus();
        } else if ($("#nmmemberemailid").val() == "") {
            // If New member emailid is empty
            CM.showAlert(ErrorText.new_member_email_blank);
            $("#nmmemberemailid").focus();
        } else if (CM.isValidEmail($("#nmmemberemailid").val())) {
            // If New member emailid is empty
            CM.showAlert(ErrorText.new_member_email_invalid);
            $("#nmmemberemailid").focus();
        } else if ($("#nmmemberphonenumber").val() == "") {
            // If New member phonenumber is empty
            CM.showAlert(ErrorText.new_member_phone_blank);
            $("#nmmemberphonenumber").focus();
        } else if ($("#nmmemberphonenumber").val().length !== 10) {
            // If New member phonenumber is not a valid mobile number
            CM.showAlert(ErrorText.new_member_phone_invalid);
            $("#nmmemberphonenumber").focus();
        } else if ($("#nmmemberaddress").val() == "") {
            // If New member address is empty
            CM.showAlert(ErrorText.new_member_address_blank);
            $("#nmmemberaddress").focus();
        } else {
            // All the fields are filled try to save the member
            // ATPM.saveMemberLocally();
            ATPM.addNewMember();
        }
    },
    saveMemberLocally: function () {
        // Method to save New Member locally
        var memberObj = {}; // Temp object to hold the member form data
        memberObj.nmName = $("#nmfullname").val(); // New member name
        memberObj.nmOrganisation = $("#nmorganisationname").val(); // New member organisation
        memberObj.nmEmail = $("#nmmemberemailid").val(); // New member emailid
        memberObj.nmPhone = $("#nmmemberphonenumber").val(); // New member phone number
        memberObj.nmAddress = $("#nmmemberaddress").val(); // New member address
        ATPM.newMemberArray.push(memberObj); // Push the data into array to use in select menu
        ATPM.updateMembersList(); // Update Member list
        ATPM.clearAddMemberForm(); // clear the fields
    },
    clearAddMemberForm: function () {
        // Method to clear Add Member form
        $("#nmfullname,#nmorganisationname,#nmmemberemailid,#nmmemberphonenumber,#nmmemberaddress").val("");
        $(".add-new-members-wrap").toggle(); // Close the Add Member form
    },
    updateMembersList: function () {
        // Method to update the member list
        var membersListHtml = ''; // Temp variable to hold select menu
        for (var i = 0; i < ATPM.newMemberArray.length; i++) {
            membersListHtml += '<option value=' + i + '>' + ATPM.newMemberArray[i].nmName + '</option>';
        }
        $("#memberslist").html(membersListHtml); // Append the Members list in the select menu
    },
    getMembersList: function (toBeSelected) {
        // Method to get the list of members
        console.warn('toBeSelected:', toBeSelected);
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.memberlist_fetching);
            $.ajax({
                url: localStorage.wsurl + "availableforteam",
                type: 'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                success: function (res) {
                    if (!res.error) {
                        // Fetching Award list successful
                        toBeSelected = (toBeSelected != '') ? toBeSelected : '';
                        ATPM.printMembersList(res.availableStudents, toBeSelected);
                    }
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },
    printMembersList: function (members, toBeSelected) {
        // Method to print the list of members
        ATPM.fetchedMembersArray = members;
        var membersListHtml = '<option value="">Select</option>';
        for (var i = 0; i < members.length; i++) {
            var member = members[i];
            membersListHtml += '<option value=' + member._id + '>' + member.name.full + '</option>';
        }
        $("#memberslist").html(membersListHtml);
        if (toBeSelected) {
            $("#memberslist").val(toBeSelected).change();
            $(".add-member-pop-up").trigger('click');
        }
    },
    selectMemberForTeam: function () {
        // Method to add a member for the team
        var selectedMemberId = $("#memberslist").val();
        var memberExistOrNot = false;
        if (selectedMemberId != "" && selectedMemberId != null) {
            // Team member has been choosed
            $.each(ATPM.selectedMembersArray, function (i, v) {
                if (v._studentId == selectedMemberId) {
                    // Member already added
                    memberExistOrNot = true;
                    return false;
                } else memberExistOrNot = false;
            });
            if (!memberExistOrNot) {
                var memberObj = {
                    '_studentId': selectedMemberId
                };
                ATPM.selectedMembersArray.push(memberObj); // push the selected team member to the group
                var temp = [];
                $.ajax({
                    url: localStorage.wsurl + "designations",
                    type: 'GET',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                    },
                    success: function (res) {
                        if (!res.error) {
                            // Fetching designations
                            res.designations.forEach((value) => {
                                temp.push(value.name);
                            });
                        }
                        ATPM.addSelectedMemberForTeam(temp);
                    }
                });
            } else
                CM.showAlert(InfoText.member_already_added);
            console.warn(ATPM.selectedMembersArray);
        } else CM.showAlert(ErrorText.member_select);
    },
    addSelectedMemberForTeam: function (designation) {
        for (var i = 0; i < ATPM.fetchedMembersArray.length; i++) {
            for (var j = 0; j < ATPM.selectedMembersArray.length; j++) {
                if (ATPM.fetchedMembersArray[i]._id == ATPM.selectedMembersArray[j]._studentId) {
                    console.warn("Matched");
                    var member = ATPM.fetchedMembersArray[i];
                }
            }
        }
        ATPM.membersArray.push({
            _studentId: member._id,
            designation: 'Member',
            level: 99,
            status: '',
  
            fullName: member.name.full,
            email: member.email,
            phone: member.phone
        });
        // console.log(member);
        ATPM.selectedMemberHtml += '<div class="member-details">'; // member holder start
        ATPM.selectedMemberHtml += '<div>'; // member name holder start
        ATPM.selectedMemberHtml += '<p>' + member.name.full + '</p>'; // member name
        ATPM.selectedMemberHtml += '</div>'; // member name holder end
        ATPM.selectedMemberHtml += '<div>'; // member option holder start
        ATPM.selectedMemberHtml += '<span><b>Member</b></span>'; // member label
        ATPM.selectedMemberHtml += '<a class="btn btn-default btn-select" style="display:inline-block; width:68%;">'; // select menu holder start
        ATPM.selectedMemberHtml += '<input type="hidden" class="btn-select-input" id="" name="" value="" />';
        ATPM.selectedMemberHtml += '<span class="btn-select-value">Select</span>';
        ATPM.selectedMemberHtml += '<span class="btn-select-arrow fa fa-caret-down" style="width:13%;"></span>'
        ATPM.selectedMemberHtml += '<ul>'; // option list
        designation.forEach((value) => {
            ATPM.selectedMemberHtml += '<li class="designation" data-id="'+ member.id + '" >'+ value +'</li>'; // option list
        });
        ATPM.selectedMemberHtml += '</ul>'; // option list
        ATPM.selectedMemberHtml += '</a>'; // select menu holder end
        ATPM.selectedMemberHtml += '</div>'; // member option holder end
        ATPM.selectedMemberHtml += '<div>'; // mobile number holder start
        ATPM.selectedMemberHtml += '<span><i class="fa fa-mobile fa-fw" aria-hidden="true" style="font-size:24px;text-indent:-8px"></i></span>'; // mobile icon
        ATPM.selectedMemberHtml += '<b>' + member.phone + '</b>'; // mobile number
        ATPM.selectedMemberHtml += '</div>'; // mobile number holder end
        ATPM.selectedMemberHtml += '<div>'; // email holder start
        ATPM.selectedMemberHtml += '<span><i class="fa fa-envelope fa-fw" aria-hidden="true"></i></span>'; // email icon
        ATPM.selectedMemberHtml += '<b>' + member.email + '</b>'; // mobile number
        ATPM.selectedMemberHtml += '</div>'; // email number holder end
        ATPM.selectedMemberHtml += '</div>'; // member holder end
        $("#selectedteammemberlist").html(ATPM.selectedMemberHtml); // refresh list
        $("li.designation").on('click', function(){
            console.log(ATPM.membersArray);
            ATPM.selectedMemberHtml = ATPM.selectedMemberHtml.replace('<span class="btn-select-value">Select</span>', '<span class="btn-select-value">' + $(this).text() + '</span>');
            if($(this).text() === 'Team Leader') {
                // console.log($('li.designation').parents().find('input#hidden_id').val());
                var id = $(this).data('id');
                ATPM.membersArray[ATPM.membersArray.length-1].designation = 'Team Leader';
                ATPM.teamLeaders.push(id)
                console.log(ATPM.teamLeaders);
            }
        });
    },
    // refreshSelectedMembersList: function (designation) {
    //     // Method to refresh the list of selected members
    //     var selectedMemberHtml = '';
    //     for (var i = 0; i < ATPM.fetchedMembersArray.length; i++) {
    //         for (var j = 0; j < ATPM.selectedMembersArray.length; j++) {
    //             if (ATPM.fetchedMembersArray[i]._id == ATPM.selectedMembersArray[j]._studentId) {
    //                 console.warn("Matched");
    //                 var member = ATPM.fetchedMembersArray[i];
    //                 selectedMemberHtml += '<div class="member-details">'; // member holder start
    //                 selectedMemberHtml += '<div>'; // member name holder start
    //                 selectedMemberHtml += '<p>' + member.name.full + '</p>'; // member name
    //                 selectedMemberHtml += '</div>'; // member name holder end
    //                 selectedMemberHtml += '<div>'; // member option holder start
    //                 selectedMemberHtml += '<span><b>Member</b></span>'; // member label
    //                 selectedMemberHtml += '<a class="btn btn-default btn-select" style="display:inline-block; width:68%;">'; // select menu holder start
    //                 selectedMemberHtml += '<input type="hidden" class="btn-select-input" id="" name="" value="" />';
    //                 selectedMemberHtml += '<span class="btn-select-value">Select</span>';
    //                 selectedMemberHtml += '<span class="btn-select-arrow fa fa-caret-down" style="width:13%;"></span>'
    //                 selectedMemberHtml += '<ul>'; // option list
    //                 designation.forEach((value) => {
    //                     selectedMemberHtml += '<li class="designation" data-id="'+ member.id + '" >'+ value +'</li>'; // option list
    //                 });
    //                 selectedMemberHtml += '</ul>'; // option list
    //                 selectedMemberHtml += '</a>'; // select menu holder end
    //                 selectedMemberHtml += '</div>'; // member option holder end
    //                 selectedMemberHtml += '<div>'; // mobile number holder start
    //                 selectedMemberHtml += '<span><i class="fa fa-mobile fa-fw" aria-hidden="true" style="font-size:24px;text-indent:-8px"></i></span>'; // mobile icon
    //                 selectedMemberHtml += '<b>' + member.phone + '</b>'; // mobile number
    //                 selectedMemberHtml += '</div>'; // mobile number holder end
    //                 selectedMemberHtml += '<div>'; // email holder start
    //                 selectedMemberHtml += '<span><i class="fa fa-envelope fa-fw" aria-hidden="true"></i></span>'; // email icon
    //                 selectedMemberHtml += '<b>' + member.email + '</b>'; // mobile number
    //                 selectedMemberHtml += '</div>'; // email number holder end
    //                 selectedMemberHtml += '</div>'; // member holder end
    //             } else console.log("No Match found");
    //         }
    //     }
    //     $("#selectedteammemberlist").html(selectedMemberHtml); // refresh list
    //     $("li.designation").on('click', function(){
    //         if($(this).text() === "Team Leader") {
    //             // console.log($("li.designation").parents().find('input#hidden_id').val());
    //             var id = $(this).data('id');
    //             ATPM.teamLeaders.push(id)
    //             console.log(ATPM.teamLeaders);
    //         }
    //     });
    // },
    saveTeam: function () {
        // Method to save a Team on the server
        var teamName = $("#teamname");
        if (teamName.val() == "") {
            CM.showAlert(ErrorText.team_name_blank);
            teamName.focus();
            return;
        }
        if (ATPM.teamLeaders.length === 0 || ATPM.teamLeaders.length > 1) {
            CM.showAlert(ErrorText.team_leader_exact_one);
            return;
        }
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.team_saving);
            // var profile = JSON.parse(localStorage.myprofile);
            var newTeamObj = {};
            newTeamObj.newTeam = {};
            newTeamObj.newTeam.name = teamName.val();
            newTeamObj.newTeam.members = ATPM.membersArray;
            newTeamObj.newTeam._teamLead = ATPM.teamLeaders[0]; 
            console.log(newTeamObj);
            $.ajax({
                url: localStorage.wsurl + "team",
                type: 'POST',
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                data: newTeamObj,
                success: function (res) {
                    if (!res.error) {
                        console.warn(JSON.stringify(res));
                        CM.redirectToPage('teamlist.html');
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },
    addNewMember: function () {
        // Method to add new member to the DB
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.team_member_saving);
            // var newMemberObj = {};
            // newMemberObj.newMember = {};
            var newMemberObj = {
                newMember: {
                    email: $("#nmmemberemailid").val(),
                    name: {
                        first: $("#nmfullname").val().split(' ')[0],
                        last: $("#nmfullname").val().split(' ')[1]
                    },
                    phone: $("#nmmemberphonenumber").val(),
                    address: $("#nmmemberaddress").val(),
                    organization: $("#nmorganisationname").val()
            }};
            $.ajax({
                url: localStorage.wsurl + "users",
                type: 'POST',
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                data: newMemberObj,
                success: function (res) {
                    if (!res.error) {
                        console.warn(JSON.stringify(res));
                        ATPM.getMembersList(res.savedUser._id);
                        $("#nmmemberemailid,#nmmemberphonenumber,#nmmemberaddress,#nmorganisationname").val(""); // clear the form
                        $(".add-new-members-wrap").toggle(); // hide the form
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    console.log(err);
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },
    addSelectedMember: function (newMember) {
        // Method to add selected member
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.team_member_saving);
            var newMemberObj = {};
            if (!newMember)
                // Adding existing Member/Student
                newMemberObj.memberId = $("#memberslist").val();
            else
                // Adding New member
                newMemberObj.memberId = newMember;
            $.ajax({
                url: localStorage.wsurl + "team/addmember/" + localStorage.selectedteam,
                type: 'PUT',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                data: newMemberObj,
                success: function (res) {
                    if (!res.error) {
                        console.warn(JSON.stringify(res));
                        // Redirect to TeamDetails page after 2s to avoid showing old data
                        setTimeout(function () {
                            CM.redirectToPage('teamdetails.html');
                        }, 2000);
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    }, addNewTeamMember: function () {
        // Method to add new member to the DB
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.team_member_saving);
            var newMemberObj = {};
            newMemberObj.newMember = {};
            newMemberObj.newMember.email = $("#nmmemberemailid").val();
            newMemberObj.newMember.name = {
                'first': $("#nmfullname").val(),
                'last': ''
            };
            newMemberObj.newMember.phone = $("#nmmemberphonenumber").val();
            newMemberObj.newMember.address = $("#nmmemberaddress").val();
            newMemberObj.newMember.organization = $("#nmorganisationname").val();
            $.ajax({
                url: localStorage.wsurl + "users",
                type: 'POST',
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                data: newMemberObj,
                success: function (res) {
                    if (!res.error) {
                        console.warn(JSON.stringify(res));
                        ATPM.addSelectedMember(res.savedUser._id);
                        $("#nmmemberemailid,#nmmemberphonenumber,#nmmemberaddress,#nmorganisationname").val(""); // clear the form
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    }
};

document.addEventListener("deviceready", function () {
    ATPM.getMembersList(false);
}, false);