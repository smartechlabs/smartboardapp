/****Common methods used across the app****/
console.warn("CM Loaded");
var CM = {
    /**Common method literal**/
    setAPIEndPoint: function () {
        // Method to set API end point
        localStorage.wsurl = "http://159.65.157.177:4000/api/v1/";
        localStorage.weburl = "http://159.65.157.177:4000/"; // this is to be used in forgot password feature alone
    },
    checkConnection: function () {
        // Method to check Internet Connection
        var networkState = navigator.connection.type;


        if (networkState == Connection.NONE)
            // No Internet connection
            return false;
        else
            return true;

    },
    showAlert: function (alertMsg) {
        // Method to show native Dialog alerts
        navigator.notification.alert(alertMsg, function () {
            console.log("Alert Closed");
        })
    },
    showLoader: function (loaderMsg) {
        // Method to show loader message
        waitingDialog.show(loaderMsg);
    },
    redirectToPage: function (pageUrl) {
        // Method to redirect to a page
        $(location).prop('href', pageUrl);
    },
    replaceNull: function (valueToCheck) {
        // Method to replace null value with 0
        if (valueToCheck === null)
            valueToCheck = 0;
        return valueToCheck;
    },
    replaceUndefined: function (valueToCheck) {
        if (typeof valueToCheck === 'undefined')
            valueToCheck = '';
        return valueToCheck;
    },
    isValidEmail: function (email) {
        // Method to check whether the mail is valid or not
        var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        var valid = emailReg.test(email);
        console.log("valid:" + valid);
        return !valid;
    },
    showLogOutPrompt: function () {
        // Method to prompt the user and warn before signing off
        navigator.notification.confirm(InfoText.logout_warning, function (buttonIndex) {
            if (buttonIndex == 1) {
                // clearing local storage when logging out
                localStorage.clear();
                // User needs to signoff so show login page
                CM.redirectToPage('index.html');
            }
        });
    },
    resetPassword: function () {
        // Method to reset password
        // Have added this method here to avoid creation of separate file for this method alone

        if (CM.checkConnection()) {
            // Internet available
            var resetEmail = $("#forgotpasswordemail").val();
            if (resetEmail === '') {
                CM.showAlert('Please enter email id to send reset details');
                return;
            } else if (CM.isValidEmail(resetEmail)) {
                CM.showAlert(ErrorText.new_member_email_invalid);
                return;
            }
            CM.showLoader(InfoText.reset_password_request);
            $.ajax({
                url: localStorage.weburl + "sendchangepassmail",
                type: 'POST',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                data: {
                    handle: resetEmail
                },
                crossDomain: true,
                success: function (res) {
                    if (res.complete) {
                        CM.showAlert(InfoText.reset_password_sent);
                        CM.redirectToPage('index.html');
                    } else
                        CM.showAlert(ErrorText.reset_password_mail_not_found);
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    }
};

var ErrorText = {
    /**Holder for having all the error messages**/
    no_internet: "Please check your Internet Connection",
    email_blank: "Please enter your Email Address",
    login_password_blank: "Please enter your Password",
    project_name_blank: "Please enter Project name",
    project_incentive_points_blank: "Please enter Incentive Points",
    project_assignee_not_selected: "Please choose an Assignee",
    project_location_empty: "Please enter Project location",
    new_member_name_blank: "Please enter Member name",
    new_member_organisation_blank: "Please enter Member Organisation",
    new_member_email_blank: "Please enter new member Email Address",
    new_member_email_invalid: "Please enter a valid Email Address",
    new_member_phone_blank: "Please enter new member Phone Number",
    new_member_phone_invalid: "Please enter a valid Phone Number",
    new_member_address_blank: "Please enter new member Address",
    new_designation_name_blank: "Please enter Designation name",
    new_designation_level_blank: "Please enter Designation level",
    new_designation_role_blank: "Please enter Role description",
    new_point_award_team_blank: "Please select Team",
    new_point_award_team_member_blank: "Please select Team Member",
    new_point_award_team_project_blank: "Please select Project",
    new_point_award_team_points_blank: "Please enter Points",
    new_goal_name_blank: "Please enter Goal name",
    new_goal_due_date_blank: "Please choose a Due Date",
    new_goal_weight_blank: "Plese enter Goal weight",
    new_goal_percentage_blank: "Please enter percentage of Goal completed",
    new_task_subject_blank: "Please enter Task Subject",
    new_task_desc_blank: "Please enter Task Description",
    new_task_start_date_blank: "Please choose Task Start date",
    new_task_end_date_blank: "Please choose Task End date",
    new_task_incentive_blank: "Please enter Task Incentive points",
    new_expense_description_blank: "Please enter Expense Description",
    new_expense_amount_blank: "Please enter Expense amount",
    member_select: "Please select a member to add to the team",
    team_name_blank: "Please name your Team",
    team_member_zero: "PPlease add at least one team member",
    team_leader_exact_one: "A Team must have EXACTLY ONE Team Leader",
    note_subject_blank: "Please enter Note Subject",
    note_description_blank: "Please enter Note Description",
    report_date_blank: "Please choose Report date",
    report_subject_blank: "Please enter Report Subject",
    report_description_blank: "Please enter Report Description",
    reset_password_mail_not_found: "Please check the email you have entered"
};

var InfoText = {
    /**Holder for having all the info messages**/
    login_verification: "Verifying your credentials",
    logout_warning: "You are about to Logout. Do you want to Continue?",
    teamlist_fetching: "Fetching Teams",
    changing_team_status: "Updating Team Status",
    projectlist_fetching: "Fetching Projects",
    project_task_status_updating: "Updating Task Status",
    project_task_assignee_updating: "Changing Assignee",
    viewtask_fetching: "Fetching selected task info",
    myprofile_fetching: "Fetching your details",
    profilelist_fetching: "Fetching Profiles",
    project_saving: "Saving Project",
    designationlist_fetching: "Fetching Designations",
    designation_saving: "Saving Designation",
    designation_updating: "Updating Designation",
    designationlist_deleting: "Deleting Designation",
    designation_delete_warning: "You are about to delete Designation. Do you want to Continue?",
    awardlist_fetching: "Fetching Awards",
    awards_fetching_members: "Fetching Members",
    point_award_saving: "Saving Point Awards",
    special_award_saving: "Saving Special Awards",
    goal_saving: "Saving Goal",
    goal_updating: "Updating Goal",
    task_saving: "Saving Task",
    expense_saving: "Saving Expense",
    project_incentive_updating: "Updating Incentive points",
    reset_password_request: "Requesting for password reset",
    reset_password_sent: "Reset password details sent to your email",
    memberlist_fetching: "Fetching Members list",
    member_already_added: "You have already added this member",
    team_saving: "Saving Team",
    team_member_saving: "Saving Member",
    note_saving: "Saving Note",
    note_updating: "Updating Note",
    team_detail_fetching: "Fetching Team details",
    changing_member_status: "Updating Member Status",
    team_member_designation_updating: "Updating Member Designation"
}


/*Initially set the web service url*/

CM.setAPIEndPoint();