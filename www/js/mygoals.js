/****My Goals page methods(mgpm)****/
console.warn("MGPM loaded");

var MGPM = {
	/**My goals page literal**/
	getGoalsFromMemory: function () {
		// Method to get the goals from Localstorage
		if (localStorage.myprofile !== null) {
			// Profile available
			console.warn("Profile details available");
			var profile = JSON.parse(localStorage.myprofile);
			MGPM.printGoals(profile.currentUser.goals);
		} else {
			// Profile details not available fetch and save again
			MPPM.fetchMyProfileDetails();
		}
	},showAddGoal:function(){
		// Method to show Add Goal page
		localStorage.goaleditting=false;
		CM.redirectToPage('addgoal.html');
	},
	editGoal: function (goalToEdit) {
		// Method to edit the Goal
		// Todo: Add this functionality
		localStorage.goaleditting=true;
		var edittingGoal={};
		var goalToEdit=$(goalToEdit);
		edittingGoal.goalId=goalToEdit.data("goalid");
		edittingGoal.goalName=goalToEdit.data("goalname");
		edittingGoal.goalDue=goalToEdit.data("goaldue");
		edittingGoal.goalWeight=goalToEdit.data("goalweight");
		edittingGoal.goalPercent=goalToEdit.data("goalpercent");
		localStorage.savedgoaltoedit=JSON.stringify(edittingGoal); // save the goal details locally to populate in Add goal screen
		CM.redirectToPage('addgoal.html');
},
	printGoals: function (goals) {
		// Method to print goals of the user
		var goalsHtml = '';
		for (var i = 0; i < goals.length; i++) {
			goalsHtml += '<div class="card my-goals_cards">'; // goal card start
			goalsHtml += '<div class="card-header row" style="margin:0">'; // goal header start
			goalsHtml += '<div class="col-xs-8" style="padding-right:5px;">'; // goal heading container start
			goalsHtml += '<h6 class="label-goal-name">Goal Name</h6>'; // goal name title
			goalsHtml += '<h4 class="name-of-goal">' + goals[i].name + '</h4>'; // goal
			goalsHtml += '</div>'; // goal heading container end
			goalsHtml += '<div class="col-xs-3" style="padding-left:3px; padding-right:12px;">'; // goals due date start
			goalsHtml += '<h6 class="label-goal-due-date">Due Date</h6>'; // goals due date label
			goalsHtml += '<h4 class="goal-due-date">' + moment(goals[i].dueDate).format("Do MMM") + '</h4>'; // goal date
			goalsHtml += '</div>'; // goals due date end
			goalsHtml += '<div class="col-xs-1" style="padding-left:5px; padding-right:5px;">'; // goal edit button holder start
			goalsHtml += '<i class="fa fa-edit edit-goal-date" data-goalid="' + goals[i]._id + '" data-goalname="' + goals[i].name + '" data-goaldue="' + goals[i].dueDate + '" data-goalweight="' + goals[i].weight + '" data-goalpercent="' + goals[i].completionPercentage + '" onclick="MGPM.editGoal(this)"></i>'; // goal date edit button
			goalsHtml += '</div>'; // goal edit button holder end
			goalsHtml += '</div>'; // goal header end
			goalsHtml += '<div class="card-block row" style="margin:0">'; // goal card body start
			goalsHtml += '<div class="col-xs-3">'; // goal weight holder start
			goalsHtml += '<h6 class="label-weight">Weight</h6>'; // goal weight label
			goalsHtml += '<h4 class="weight-count">' + goals[i].weight + '</h4>'; // goal weight
			goalsHtml += '</div>'; // goal weight holder end
			goalsHtml += '<div class="col-xs-9">'; // goal progress block start
			goalsHtml += '<h6 class="label-progress">% Progress</h6>'; // goal progress label
			goalsHtml += '<div class="progress goal_progress-bar">'; // progress bar holder start
			goalsHtml += '<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="' + goals[i].completionPercentage + '" aria-valuemin="0" aria-valuemax="100" style="width:60%"></div>'; // progress bar
			goalsHtml += '</div>'; // progress bar holder end
			goalsHtml += '</div>'; // goal progress block end
			goalsHtml += '</div>'; // goal card body end
			goalsHtml += '</div>'; // goal card end
		}
		$("#goalslist").html(goalsHtml); // fill the Goals list
	}
};
document.addEventListener("deviceready", MGPM.getGoalsFromMemory, false);