/****Add Note page methods(anpm)****/
console.warn("ANPM loaded");
var noteSub = $("#notesubject");
var noteDes = $("#notedescription");
var ANPM = {
    /**Add Note page literal**/
    validateNoteForm: function () {
        // Method to validate Add Note form
        /*if (noteSub.val() === '') {
            CM.showAlert(ErrorText.note_subject_blank);
            noteSub.focus();
        } else*/ if (noteDes.val() === '') {
            CM.showAlert(ErrorText.note_description_blank);
            noteDes.focus();
        } else {
            if (typeof localStorage.selectedNoteData !== 'undefined')
                ANPM.updateNote();
            else
                ANPM.saveNote();
        }
    },
    fillNotes: function () {
        // Method to prefill Notes fields
        if (typeof localStorage.selectedNoteData !== 'undefined') {
            // This is edit so fill data
            var savedNoteData = JSON.parse(localStorage.selectedNoteData);
            // noteSub.val(savedNoteData.notedes);
            noteDes.val(savedNoteData.notedes);
        }
    },
    updateNote: function () {
        // Method to Update/Edit Note
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.note_updating);
            var newNoteObj = {};
            var savedNoteData = JSON.parse(localStorage.selectedNoteData);
            newNoteObj.noteid = savedNoteData.noteid;
            newNoteObj.note = noteSub.val()||'';
            newNoteObj.desc = noteDes.val();
            $.ajax({
                url: localStorage.wsurl + "tasks/notes/" + newNoteObj.noteid,
                type: 'PUT',
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                data: newNoteObj,
                success: function (res) {
                    if (!res.error) {
                        console.warn(JSON.stringify(res));
                        noteSub.val(''); // clear fields
                        noteDes.val('');
                        CM.redirectToPage('viewtask.html'); // redirect back to tasks view page
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },
    saveNote: function () {
        // Method to save Note
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.note_saving);
            var newNoteObj = {};
            newNoteObj.taskid = localStorage.selectedtaskid;
            newNoteObj.note = noteSub.val()||'';
            newNoteObj.desc = noteDes.val();
            $.ajax({
                url: localStorage.wsurl + "tasks/notes/" + newNoteObj.taskid,
                type: 'POST',
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                data: newNoteObj,
                success: function (res) {
                    if (!res.error) {
                        console.warn(JSON.stringify(res));
                        noteSub.val(''); // clear fields
                        noteDes.val('');
                        CM.redirectToPage('viewtask.html'); // redirect back to tasks view page
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    }
};


document.addEventListener("deviceready", ANPM.fillNotes, false);