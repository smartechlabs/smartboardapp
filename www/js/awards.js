/****Awards list page methods(alm)****/
console.warn("ALM loaded");
var ALM = {
    /**Awards list page literal**/
    fetchAwardsList: function () {
        // Method to get the Awards of the user
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.awardlist_fetching);
            $.ajax({
                url: localStorage.wsurl + "awards",
                type: 'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                success: function (res) {
                    if (!res.error) {
                        // Fetching Award list successful
                        ALM.printPointAwardList(res.pointsAwarded,res.specialAwards);
                    }
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },
    printPointAwardList: function (pointAwards,specialAwards) {
        // Method to print the list of point awards
        var pointAwardsHtml = '';
        for (var i = 0; i < pointAwards.length; i++) {
            var awardDate = '-';
            var eventName = '-';
            var memberName = '-';
            var taskName = '-';
            if (pointAwards[i]._projectId !== null && typeof pointAwards[i]._projectId !=='undefined') {
                // Project details available
                eventName = pointAwards[i]._projectId.name; // event name
            }
            if (pointAwards[i]._studentId !== null && typeof pointAwards[i]._studentId !== 'undefined') {
                // Member details available
                memberName = pointAwards[i]._studentId.name.full; // member name
            }
            if (pointAwards[i]._teamId !== null && typeof pointAwards[i]._teamId !== 'undefined') {
                // Team details available
                teamName = pointAwards[i]._teamId.name; // team name
            }
            if (pointAwards[i]._taskId !== null && typeof pointAwards[i]._taskId !== 'undefined') {
                // Task details available
                taskName = pointAwards[i]._taskId.name; // task name
                awardDate = moment(pointAwards[i]._taskId.startDate).format('Do MMMM');
            }
            pointAwardsHtml += '<div class="li-wrap">'; // award block start
            pointAwardsHtml += '<li class="list-group-item">'; // date li start
            pointAwardsHtml += '<div class="labels">Date</div>'; // date label
            pointAwardsHtml += '<div class="contents">' + awardDate + '</div>'; // date
            pointAwardsHtml += '</li>'; // date li end
            pointAwardsHtml += '<li class="list-group-item">'; // event li start
            pointAwardsHtml += '<div class="labels">Event</div>'; // event label
            pointAwardsHtml += '<div class="contents">' + eventName + '</div>'; // event name
            pointAwardsHtml += '</li>'; // event li end
            pointAwardsHtml += '<li class="list-group-item">'; // member li start
            pointAwardsHtml += '<div class="labels">Member</div>'; // member label
            pointAwardsHtml += '<div class="contents">' + memberName + '</div>'; // member name
            pointAwardsHtml += '</li>'; // member li end
            pointAwardsHtml += '<li class="list-group-item">'; // team li start
            pointAwardsHtml += '<div class="labels">Team</div>'; // team label
            pointAwardsHtml += '<div class="contents">' + teamName + '</div>'; // team name
            pointAwardsHtml += '</li>'; // team li end
            pointAwardsHtml += '<li class="list-group-item">'; // task li start
            pointAwardsHtml += '<div class="labels">Task</div>'; // task label
            pointAwardsHtml += '<div class="contents">' + taskName + '</div>'; // task name
            pointAwardsHtml += '</li>'; // task li end
            pointAwardsHtml += '<li class="list-group-item">'; // point li start
            pointAwardsHtml += '<div class="labels">Points</div>'; // point label
            pointAwardsHtml += '<div class="contents">' + pointAwards[i].points + '</div>'; // point name
            pointAwardsHtml += '</li>'; // point li end
            pointAwardsHtml += '</div>'; // award block start
        }
        $("#pointawardlist").html(pointAwardsHtml); // Update the UI with point awards
        ALM.printSpecialAwards(specialAwards); // Print special awards HTML
    },printSpecialAwards:function(specialAwards){
        // Method to print special awards
        var specialAwardsHtml='';
        for(var i=0;i<specialAwards.length;i++){
            var memberName='-';
            var eventName='-';
            var teamName='-';
            var taskName='-';
            var awardDate =moment(specialAwards[i].date).format('Do MMMM')|| '-';
            if (specialAwards[i]._studentId !== null && typeof specialAwards[i]._studentId !== 'undefined') {
                // Member details available
                memberName = specialAwards[i]._studentId.name.full; // member name
            }
            if (specialAwards[i]._projectId !== null && typeof specialAwards[i]._projectId !=='undefined') {
                // Event details available
                eventName = specialAwards[i]._projectId.name; // event name
            }
            if (specialAwards[i]._taskId !== null && typeof specialAwards[i]._taskId !== 'undefined') {
                // Task details available
                taskName = specialAwards[i]._taskId.name; // task name
            }
            specialAwardsHtml+='<div class="li-wrap">'; // special award block start
            specialAwardsHtml+='<li class="list-group-item">'; // date li start
            specialAwardsHtml+='<div class="labels">Date</div>'; // date label
            specialAwardsHtml+='<div class="contents">'+awardDate+'</div>'; // date
            specialAwardsHtml+='</li>'; // date li end
            specialAwardsHtml+='<li class="list-group-item">'; // event li start
            specialAwardsHtml+='<div class="labels">Event</div>'; // event label
            specialAwardsHtml+='<div class="contents">'+eventName+'</div>'; // event name
            specialAwardsHtml+='</li>'; // event li end
            specialAwardsHtml+='<li class="list-group-item">'; // member li start
            specialAwardsHtml+='<div class="labels">Member</div>'; // member label
            specialAwardsHtml+='<div class="contents">'+memberName+'</div>'; // member name
            specialAwardsHtml+='</li>'; // member li end
            specialAwardsHtml+='<li class="list-group-item">'; // team li start
            specialAwardsHtml+='<div class="labels">Team</div>'; // team label
            specialAwardsHtml+='<div class="contents">'+specialAwards[i]._teamId.name+'</div>'; // team name
            specialAwardsHtml+='</li>'; // team li end
            specialAwardsHtml+='<li class="list-group-item">'; // task li start
            specialAwardsHtml+='<div class="labels">Task</div>'; // task label
            specialAwardsHtml+='<div class="contents">'+taskName+'</div>'; // task name
            specialAwardsHtml+='</li>'; // task li end
            specialAwardsHtml+='<li class="list-group-item">'; // award li start
            specialAwardsHtml+='<div class="labels">Award</div>'; // award label
            specialAwardsHtml+='<div class="contents">'+specialAwards[i].award+'</div>'; // award name
            specialAwardsHtml+='</li>'; // award li end
            specialAwardsHtml+='</div>'; // special award block end
        }
        $("#specialawardlist").html(specialAwardsHtml); // Update the UI with point awards
        ALM.fetchTeamList(); // Fetch Team list and fill dropdown
    },fetchTeamList: function() {
        // Method to fetch team list
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.teamlist_fetching);
            $.ajax({
                url: localStorage.wsurl + "myteams",
                type: 'GET',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                success: function(res) {
                    if (!res.error) {
                        // Fetching Team list successful
                        var teamListHtml='<option value="Select">Select</option>';
                        for(var i=0;i<res.teams.length;i++){
                            teamListHtml+='<option value='+res.teams[i]._id+'>'+res.teams[i].name+'</option>';
                        }
                        $("#pointawardteam,#specialawardteam").html(teamListHtml);

                    }
                    waitingDialog.hide();
                },
                error: function(err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },fetchTeamMemberList:function(selectedTeam){
        // Method to fetch the list of Team members based on the selected Team
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.awards_fetching_members);
            $.ajax({
                url: localStorage.wsurl + "/awards/fetchTeamDetails/"+$(selectedTeam).val(),
                type: 'POST',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },                
                success: function (res) {
                    if (!res.error) {
                        var membersListHtml='<option value="">Entire Team (No Particular Member)</option>';
                        var projectListHtml='<option value="">No Particular Project</option>';
                        for(var i=0;i<res.teamMembers.length;i++){
                            // Form Members html
                            membersListHtml+='<option value="'+res.teamMembers[i].id+'">'+res.teamMembers[i].name+'</option>';
                        }
                        for(var i=0;i<res.teamProjects.length;i++){
                            // Form Projects html
                            projectListHtml+='<option value="'+res.teamProjects[i].id+'">'+res.teamProjects[i].name+'</option>';
                        }

                       if($(selectedTeam).attr('id')=='pointawardteam'){
                            // Team name changed in Points award
                            console.warn(membersListHtml);
                            $("#pointawardteammember").html(membersListHtml); // Update Members list in Point awards tab
                            $("#pointawardteamproject").html(projectListHtml); // Update Projects list in Point awards tab
                       }else if($(selectedTeam).attr('id')=='specialawardteam'){
                            // Team name changed in Special award
                            console.warn("specialawardteammember");
                            $("#specialawardteammember").html(membersListHtml); // Update Members list in Special awards tab
                            $("#specialawardteamproject").html(projectListHtml); // Update Projects list in Special awards tab
                       }
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },validatePointAward:function(){
        // Method to validate point award form
        if($("#pointawardteam").val()==""){
            // Team not selected
            CM.showAlert(ErrorText.new_point_award_team_blank);
        } else if($("#pointstoaward").val()=="") {
            // Team points not entered
            CM.showAlert(ErrorText.new_point_award_team_points_blank);
        } else {
            // All the fields are filled let's save point award
            ALM.savePointAward();
        }
    },savePointAward:function(){
        // Method to save Point award in DB
         if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.point_award_saving);
            $.ajax({
                url: localStorage.wsurl + "/awards/points",
                type: 'POST',
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                }, 
                data:{
                    team: $("#pointawardteam").val(),
                    member: ($("#pointawardteammember").val() === "") ? null : $("#pointawardteammember").val(),
                    project: ($("#pointawardteamproject").val() === "") ? null : $("#pointawardteamproject").val(),
                    isAwardingPoints: 'yes',
                    points: $("#pointstoaward").val()
                },               
                success: function (res) {
                    if (!res.error) {
                        // Todo: Show success message
                        ALM.fetchAwardsList();
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },validateSpecialAward:function() {
        // Method to validate special award form
        if($("#specialawardteam").val() == ""){
            // Team not selected
            CM.showAlert(ErrorText.new_point_award_team_blank);
        } else if($("#specialawardname").val()=="") {
            // Team points not entered
            CM.showAlert(ErrorText.new_point_award_team_points_blank);
        } else {
            // All the fields are filled let's save point award
            ALM.saveSpecialAward();
        }
    },saveSpecialAward:function(){
        // Method to save Special award in DB
         if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.special_award_saving);
            var award = {
                team: $("#specialawardteam").val(),
                member: ($("#specialawardteammember").val() === "") ? null : $("#specialawardteammember").val(),
                project: ($("#specialawardteamproject").val() === "") ? null: $("#specialawardteamproject").val(),
                isAwardingPoints: 'no',
                points: null,
                award: $("#specialawardname").val()
            };
            console.log(award);
            $.ajax({
                url: localStorage.wsurl + "awards/specialawards",
                type: 'POST',
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                }, 
                data: award,
                success: function (res) {
                    if (!res.error) {
                        // Todo: Show success message
                        ALM.fetchAwardsList();
                    } else 
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    console.log(err);
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    }
};

document.addEventListener("deviceready", ALM.fetchAwardsList, false);