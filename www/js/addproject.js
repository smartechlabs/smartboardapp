/****Add project page methods(appm)****/
console.warn("APPM Loaded");
var APPM = {
    /**Add Project page method literal**/
    validateAddProjectForm: function () {
        //Method to validate Add Project form
        if ($("#appname").val() == "") {
            // If project name is empty
            CM.showAlert(ErrorText.project_name_blank);
            $("#appname").focus();
        } else if ($("#appoints").val() == "") {
            // If project incentive points are empty
            CM.showAlert(ErrorText.project_incentive_points_blank);
            $("#appoints").focus();
        } else if ($("#appdate").val() == "") {
            // If project date is empty
            CM.showAlert(ErrorText.project_incentive_points_blank);
            $("#appdate").focus();
        } else if ($("#appassignto").val() == "select") {
            // If project assignto value is select
            CM.showAlert(ErrorText.project_assignee_not_selected);
            $("#appassignto").focus();
        } else if ($("#applocation").val() == "") {
            // If project location is empty
            CM.showAlert(ErrorText.project_location_empty);
            $("#applocation").focus();
        } else {
            // All the fields are filled try to save project
            APPM.saveProject();
        }
    },
    saveProject: function () {
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.project_saving);
            var newProject = {
                name: $("#appname").val(),
                dateTime: moment($("#appdate").val()).format('DD/MM/YYYY').toString(),
                _teamId: $("#appassignto").val(),
                location: $("#applocation").val(),
                incentivePoints: $("#appoints").val()
            };
            $.ajax({
                url: localStorage.wsurl + "projects",
                type: 'POST',
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                data: {
                    newProject: newProject
                },
                success: function (res) {
                    if (!res.error) {
                        console.warn(JSON.stringify(res));
                        CM.redirectToPage('projects.html');
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    console.log(err);
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },
    fetchTeamList: function () {
        // Method to fetch team list
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.teamlist_fetching);
            $.ajax({
                url: localStorage.wsurl + "myteams",
                type: 'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                success: function (res) {
                    if (!res.error) {
                        // Fetching Team list successful fill the Assignee menu
                        var teamListOptions = '<option value="select">Select</option>';
                        for (var i = 0; i < res.teams.length; i++) {
                            teamListOptions += '<option value="' + res.teams[i]._id + '">' + res.teams[i].name + '</option>';
                        }
                        $("#appassignto").html(teamListOptions);

                    }
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    }
};
document.addEventListener("deviceready", APPM.fetchTeamList, false);