/****View task page methods(plm)****/
console.warn("VTPM loaded");

var VTPM = {
    /**View task page literal**/
    fetchTaskInfo: function () {
        // Method to fetch task info
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.viewtask_fetching);
            $.ajax({
                url: localStorage.wsurl + "tasks/" + localStorage.selectedtaskid,
                type: 'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                success: function (res) {
                    if (!res.error) {
                        console.warn(JSON.stringify(res));
                        $("#vtname").text(res.title); // Fill Task name in header
                        if (typeof res.task._assignedTo !== 'undefined')
                            $("#vtassignedtoname").text(res.task._assignedTo.name.full); // Fill Assignee name
                        $("#vttotalincentive").text(res.task.incentivePoints); // Fill Incentive points
                        var taskStartDate = moment(res.task.startDate).format('D');
                        var taskEndDate = moment(res.task.endDate).format('D MMM');
                        $("#vtstartenddate").text('Start & End Date:' + taskStartDate + ' to ' + taskEndDate);
                        $("#vtprogress").text(res.task.status);
                        $("#vtdescription").text(res.task.desc);
                        VTPM.listExpenses(res.task._expenses, res.task._notes, res.task._reports);
                    }
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },
    listNotes: function (notes, reports) {
        // Method to list the fetched Notes
        var notesHtml = '';
        for (var i = 0; i < notes.length; i++) {
            notesHtml += '<div class="card card-block notes-card-light-bg">'; // note card start
            notesHtml += '<h6 class="posted-by">Posted by: <span>' + notes[i]._addedBy.name.full + '</span></h6>'; // poster name
            notesHtml += '<blockquote>'; // note holder start
            notesHtml += '<div class="row" style="margin:0">'; // note inner holder start
            notesHtml += '<div class="col-xs-3 task-date">'; // note date holder start
            notesHtml += '<h6>' + moment(notes[i].addedOn).format('MMM') + '</h6><p>' + moment(notes[i].addedOn).format('DD') + '</p>'; // note Month and Date
            notesHtml += '</div>'; // note date holder end
            notesHtml += '<div class="col-xs-8" style="padding:0 5px">'; // note description holder start
            notesHtml += '<p class="task-notes">' + notes[i].desc + '</p>'; // note description
            notesHtml += '</div>'; // note description holder end
            notesHtml += '<div class="col-xs-1" style="padding:0 5px">'; // Edit button holder start
            notesHtml += '<i class="fa fa-edit task-notes-edit" data-noteid="' + notes[i]._id + '" data-notesub="' + notes[i].desc + '" data-notedes="' + notes[i].desc + '" onclick="VTPM.showNotesPage(this)"></i>'; // Edit button
            notesHtml += '</div>'; // Edit button holder end
            notesHtml + '</div>'; // note inner holder end
            notesHtml += '</blockquote>'; // note holder end
            notesHtml += '</div>'; // note card end
        }
        notesHtml += '<button type="button" class="btn btn-lg btn-block green-btn" style="margin-top:10px;" onclick="VTPM.showNotesPage()">Add Note</button>';
        $("#notes").html(notesHtml);
        VTPM.listReports(reports);
    },
    showNotesPage: function (editParams) {
        // Method to show Notes page
        if (typeof editParams !== 'undefined') {
            // This is edit so save those details
            var editNoteParams = $(editParams);
            var selectedNoteData = {};
            selectedNoteData.noteid = editNoteParams.data('noteid');
            selectedNoteData.notesub = editNoteParams.data('notesub');
            selectedNoteData.notedes = editNoteParams.data('notedes');
            localStorage.selectedNoteData = JSON.stringify(selectedNoteData);
        } else localStorage.removeItem('selectedNoteData');
        CM.redirectToPage("addnote.html");
    },
    listReports: function (reports) {
        // Method to list the fetched Reports
        // Todo: Make it Dynamic
        var reportsHtml = '';
        for (var i = 0; i < reports.length; i++) {
            reportsHtml += '<div class="card card-block notes-card-light-bg">'; // report card start
            reportsHtml += '<h6 class="posted-by">Posted by: <span>' + reports[i]._addedBy.name.full + '</span></h6>'; // poster name
            reportsHtml += '<blockquote>'; // report holder start
            reportsHtml += '<div class="row" style="margin:0">'; // report inner holder start
            reportsHtml += '<div class="col-xs-3 task-date">'; // report date holder start
            reportsHtml += '<h6>' + moment(reports[i].addedOn).format('MMM') + '</h6><p>' + moment(reports[i].addedOn).format('DD') + '</p>'; // report Month and Date
            reportsHtml += '</div>'; // report date holder end
            reportsHtml += '<div class="col-xs-8" style="padding:0 5px">'; // report description holder start
            reportsHtml += '<p class="task-notes">' + reports[i].desc + '</p>'; // report description
            reportsHtml += '</div>'; // report description holder end
            reportsHtml += '<div class="col-xs-1" style="padding:0 5px">'; // Edit button holder start
            reportsHtml += '<i class="fa fa-edit task-notes-edit" data-reportdate="' + reports[i].dueDate + '" data-reportid="' + reports[i]._id + '" data-reportsub="' + reports[i].name + '" data-reportdesc="' + reports[i].desc + '" onclick="VTPM.showReportsPage(this)"></i>'; // Edit button
            reportsHtml += '</div>'; // Edit button holder end
            reportsHtml += '</div>'; // report inner holder end
            reportsHtml += '</blockquote>'; // report holder end
            reportsHtml += '</div>'; // report card end
        }
        reportsHtml += '<button type="button" class="btn btn-lg btn-block green-btn" style="margin-top:10px;" onclick="VTPM.showReportsPage()">Add Report</button>';
        $("#reports").html(reportsHtml);
    },
    showReportsPage: function (editParams) {
        // Method to show Add report page
        if (typeof editParams !== 'undefined') {
            // This is edit so save those details
            var editNoteParams = $(editParams);
            var selectedReportData = {};
            selectedReportData.reportid = editNoteParams.data('reportid');
            selectedReportData.reportsub = editNoteParams.data('reportsub');
            selectedReportData.reportdesc = editNoteParams.data('reportdesc');
            selectedReportData.reportdate = editNoteParams.data('reportdate');
            localStorage.selectedReportData = JSON.stringify(selectedReportData);
        } else localStorage.removeItem('selectedReportData');
        CM.redirectToPage("addreport.html");
    },
    listExpenses: function (expenses, notes, reports) {
        // Method to list the fetched Expenses
        // Todo: Make it Dynamic

        var expensesHtml = "";
        for (var i = 0; i < expenses.length; i++) {
            var billCompleted = "";
            var billPending = "";
            var billInProgress = "";
            if (expenses[i].status == 'Complete')
                billCompleted = "selected";
            else if (expenses[i].status == 'Pending')
                billPending = "selected";
            else
                billInProgress = "selected";

            expensesHtml += '<div class="card my-goals_cards">'; // expense card start
            expensesHtml += '<div class="card-header row" style="margin:0">'; // card header start
            expensesHtml += '<div class="col-xs-7" style="padding:0 5px 0 10px;">'; // member info holder start
            expensesHtml += '<h6 class="label-member">Member :</h6>'; // member label
            expensesHtml += '<h4 class="name-of-member">' + expenses[i]._addedBy.name.full + '</h4>'; // member name
            expensesHtml += '</div>'; // member info holder end
            expensesHtml += '<div class="col-xs-5" style="padding:0 15px 0 3px;">'; // amount holder start
            expensesHtml += '<h6 class="label-amount">Amount</h6>'; // amount label
            expensesHtml += '<h4 class="amount-total">&#8377 ' + expenses[i].amount + '</h4>'; // amount
            expensesHtml += '</div>'; // amount holder end
            expensesHtml += '</div>'; // card header end
            expensesHtml += '<div class="card-block row expenses-card">'; // expense card body start
            expensesHtml += '<div class="col-xs-10" style="padding:0 9px;">'; // expense description holder start
            expensesHtml += '<p class="member-note">Need to add this in API.</p>'; // expense description
            expensesHtml += '</div>'; // expense description holder end
            expensesHtml += '<div class="clearfix"></div>'; // simple space
            expensesHtml += '<div class="row top-border">'; // expense list container start
            expensesHtml += '<form>'; // expense list form start
            /*expensesHtml+='<div class="col-xs-6" style="padding-left:10px;">'; // expense Item start
            expensesHtml+='<div class="bill">'; // expense bill title holder start
            expensesHtml+='<label>Bill</label>'; // bill label
            expensesHtml+='<span>Hotel_receipt.pdf</span>'; // bill name
            expensesHtml+='</div>'; // expense bill title holder end
            expensesHtml+='</div>'; // expense Item end*/
            expensesHtml += '<div class="col-xs-12" style="padding-right:10px">'; //expense status changer block start
            expensesHtml += '<div class="form-group task-status-select" style="margin:-3px auto 0;">'; // status holder start
            expensesHtml += '<label>Status</label>'; // status label
            expensesHtml += '<select class="form-control statusselect" style="height:34.5px" id="select' + expenses[i].id + '">'; // status select menu start
            expensesHtml += '<option ' + billInProgress + '>Select</option><option value="Complete" ' + billCompleted + '>Complete</option><option value="Pending" ' + billPending + '>Pending</option>';
            expensesHtml += '</select>'; // status select menu end
            expensesHtml += '</div>'; // status holder end
            expensesHtml += '</div>'; // expense status changer block end
            expensesHtml += '</form>'; // expense list form end
            expensesHtml += '</div>'; // expense list container end
            expensesHtml += '</div>'; // expense card body end
            expensesHtml += '</div>'; // expense card end
        }
        expensesHtml += '<button type="button" class="btn btn-lg btn-block green-btn" style="margin-top:10px;" onclick=CM.redirectToPage("addexpense.html")>Add Expenses</button>';
        $("#expenses").html(expensesHtml);
        VTPM.listNotes(notes, reports);
    }
};

document.addEventListener("deviceready", VTPM.fetchTaskInfo, false);