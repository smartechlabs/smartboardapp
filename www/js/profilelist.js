/****Profile List page methods(mppm)****/
console.warn("PLPM loaded");

var PLPM = {
    /**Profile list page literal**/
    fetchProfileList: function () {
        // Method to fetch all the profiles
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.profilelist_fetching);
            $.ajax({
                url: localStorage.wsurl + "profile",
                type: 'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                success: function (res) {
                    if (!res.error) {
                        // Fetching user profile successful
                        PLPM.printProfileList(res.users);
                    }
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },
    printProfileList: function (profileList) {
        // Method to print the list of profiles
        var profileListHtml = '';
        for (var i = 0; i < profileList.length; i++) {
            console.warn(JSON.stringify(profileList[i]));
            profileListHtml += '<div class="card directory_cards">'; // profile container start
            profileListHtml += '<div class="card-header row" style="margin:0">'; // profile header start
            profileListHtml += '<div class="col-xs-7" style="padding-right:5px;">'; // profile heading block start
            profileListHtml += '<h6 class="label-designation">Name & Designation</h6>'; // name and designation heading
            profileListHtml += '<h4 class="name-of-person">' + profileList[i].name.full + ' <span>(Student)</span></h4>'; // name and designation // Todo: Change Designation param
            profileListHtml += '</div>'; // profile heading block end
            profileListHtml += '<div class="col-xs-5" style="padding-left:3px; padding-right:12px;">'; // location and department block start
            profileListHtml += '<h6 class="label-location">Location: <span>' + (profileList[i].address || 'N/A') + '</span></h6>'; // location
            profileListHtml += '<h6 class="label-department">Department: <span>' + (profileList[i].organization || 'N/A') + '</span></h6>'; // department 
            profileListHtml += '</div>'; // location and department block end
            profileListHtml += '</div>'; // profile header end
            profileListHtml += '<div class="card-block">'; // profile body start
            profileListHtml += '<ul class="list-group profile-info">'; // contact block start
            profileListHtml += '<li class="list-group-item"><i class="fa fa-envelope"></i> ' + profileList[i].email + '</li>'; // email
            profileListHtml += '<li class="list-group-item"><i class="fa fa-phone" style="padding-right:12px;"></i> ' + (profileList[i].phone || 'N/A') + '</li>'; // mobile
            profileListHtml += '</ul>'; // contact block end
            profileListHtml += '</div>'; // profile body end
            profileListHtml += '</div>'; // profile container end
        }
        $("#profilelist").html(profileListHtml); // append the details to the profile block
    }
};

document.addEventListener("deviceready", PLPM.fetchProfileList, false);