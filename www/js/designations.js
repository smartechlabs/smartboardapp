/****Designation list page methods(dlm)****/
console.warn("DLM loaded");
var DLM = {
    /**Designation list page literal**/
    fetchDesignationList: function () {
        // Method to get the Designation of the user
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.designationlist_fetching);
            $.ajax({
                url: localStorage.wsurl + "designations",
                type: 'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                success: function (res) {
                    if (!res.error) {
                        // Fetching Designation list successful
                        DLM.printDesignationList(res.designations);
                    }
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },
    printDesignationList: function (designations) {
        // Method to print list of Designations of the user
        var designationListHtml = '';
        for (var i = 0; i < designations.length; i++) {
            designationListHtml += '<div class="designation-wrap">'; // designation holder start
            designationListHtml += '<div>'; // designation block start
            designationListHtml += '<label>Designation'; // designation label start
            designationListHtml += '<span class="pull-right">'; // designation option holder start
            if (designations[i].name !== 'Member') {
                designationListHtml += '<i class="fa fa-edit edit-desig" data-designationid="' + designations[i]._id + '" data-designationname="' + designations[i].name + '" data-designationlevel="' + designations[i].level + '" data-designationrole="' + designations[i].roleDesc + '" onclick="DLM.fillDesignationForm(this)"></i> &nbsp; &nbsp; &nbsp;'; // designation edit button
                designationListHtml += '<i class="fa fa-trash delete-desig" data-designationid="' + designations[i]._id + '" onclick="DLM.showDeleteDesignationPrompt(this)"></i>'; // designation delete button
            }
            designationListHtml += '</span>'; // designation option holder end
            designationListHtml += '</label>'; // designation label end
            designationListHtml += '<h4>' + designations[i].name + '</h4>'; // designation
            designationListHtml += '</div>'; // designation block end
            designationListHtml += '<div>'; // designation level and role block start
            designationListHtml += '<span class="level-wrap">'; // level holder start
            designationListHtml += '<label>Level</label>'; // level label
            designationListHtml += '<h4 class="text-center"><b>' + designations[i].level + '</b></h4>'; // level
            designationListHtml += '</span>'; // level holder end
            designationListHtml += '<span class="role-wrap">'; // role block start
            designationListHtml += '<label>Role</label>'; // role label
            designationListHtml += '<h4>' + designations[i].roleDesc + '</h4>'; // role
            designationListHtml += '</span>'; // role block end
            designationListHtml += '</div>'; // designation level and role block end
            designationListHtml += '</div>'; // designation holder end
        }
        $("#designationlist").html(designationListHtml); // update the UI
    },
    validateAddDesignationForm: function (saveOrUpdate) {
        // Method to validate Add Designation form
        if ($.trim($("#designationname").val()) == "") {
            // if designation name is empty
            CM.showAlert(ErrorText.new_designation_name_blank);
            $("#designationname").focus();
        } else if ($.trim($("#designationlevel").val()) == "") {
            // if designation level is empty
            CM.showAlert(ErrorText.new_designation_level_blank);
            $("#designationlevel").focus();
        } else if ($.trim($("#designationrole").val()) == "") {
            // if designation role is empty
            CM.showAlert(ErrorText.new_designation_role_blank);
            $("#designationrole").focus();
        } else {
            if (saveOrUpdate === "save")
                // all the needed fields are filled we can save the designation
                DLM.saveDesignation();
            if (saveOrUpdate === "update")
                // all the needed fields are filled we can update the designation
                DLM.updateDesignation();
        }
    },
    saveDesignation: function () {
        // Method to save designation in DB

        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.designation_saving);
            var designationObj = {}; // Temp object to store the designation details
            designationObj.newDesignation = {};
            designationObj.newDesignation.name = $.trim($("#designationname").val()); // Designation name
            designationObj.newDesignation.level = parseInt($.trim($("#designationlevel").val())); // Designation level
            designationObj.newDesignation.roleDesc = $.trim($("#designationrole").val()); // Designation description

            $.ajax({
                url: localStorage.wsurl + "designations",
                type: 'POST',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                data: designationObj,
                success: function (res) {
                    if (!res.error) {
                        console.warn(JSON.stringify(res));
                        DLM.resetAddDesignationForm(); // reset the add form
                        DLM.fetchDesignationList(); // fetch the designation list
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },updateDesignation: function () {
        // Method to save designation in DB

        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.designation_updating);
            var designationObj = {}; // Temp object to store the designation details
            designationObj.newDesignation = {};
            designationObj.newDesignation.name = $.trim($("#designationname").val()); // Designation name
            designationObj.newDesignation.level = parseInt($.trim($("#designationlevel").val())); // Designation level
            designationObj.newDesignation.roleDesc = $.trim($("#designationrole").val()); // Designation description
            $.ajax({
                url: localStorage.wsurl + "designations/"+$("#updatedesignationbtn").data("designationid"),
                type: 'PUT',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                data: designationObj,
                success: function (res) {
                    if (!res.error) {
                        console.warn(JSON.stringify(res));
                        // Todo: Show toast alert to say success message
                        DLM.resetAddDesignationForm(); // reset the add form
                        DLM.fetchDesignationList(); // fetch the designation list
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },resetAddDesignationForm:function(){
        // Method to reset the designation form
         $("#designationname,#designationlevel,#designationrole").val(""); // clear Designation form fields
         $("#adddesignationform").toggle(); // show or hide add designation form
         $("#savedesignationbtn").show(); // show save button
         $("#updatedesignationbtn").hide(); // hide update button
    },
    deleteDesignation: function (selectedDesignation) {
        // Method to delete designation from DB
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.designationlist_deleting);
            $.ajax({
                url: localStorage.wsurl + "designations/" + $(selectedDesignation).data("designationid"),
                type: 'DELETE',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                success: function (res) {
                    if (!res.error) {
                        // Deleting Designation successful Todo: Add Toast alert to say delete success
                        DLM.fetchDesignationList();
                    }
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },
    showDeleteDesignationPrompt: function (selectedDesignation) {
        // Method to prompt user before deleting the designation
        navigator.notification.confirm(InfoText.designation_delete_warning, function (buttonIndex) {
            if (buttonIndex == 1) {
                // User needs to delete the designation
                DLM.deleteDesignation(selectedDesignation);
            }
        });
    },
    fillDesignationForm: function (selectedDesignation) {
        // Method to fill the designation form while editing
        $("#designationname").val($(selectedDesignation).data("designationname")); // fill designation name
        $("#designationlevel").val($(selectedDesignation).data("designationlevel")); // fill designation level
        $("#designationrole").val($(selectedDesignation).data("designationrole")); // fill designation role
        $("#updatedesignationbtn").data("designationid", $(selectedDesignation).data("designationid")); // add designationid of the editing designation
        $("#savedesignationbtn").hide(); // hide save button
        $("#adddesignationform,#updatedesignationbtn").show(); // show form and update button
        $(window).scrollTo($("#adddesignationform")); // scroll to the addform
    }
};

document.addEventListener("deviceready", DLM.fetchDesignationList, false);