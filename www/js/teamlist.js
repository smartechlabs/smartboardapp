/****Team list page methods(tlm)****/
console.warn("TLM loaded");
var TLM = {
    /**Team list page literal**/
    fetchTeamList: function () {
        document.addEventListener("backbutton", function () {
            navigator.app.exitApp();
        }, false);
        // Method to fetch team list
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.teamlist_fetching);
            $.ajax({
                url: localStorage.wsurl + "myteams",
                type: 'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                success: function (res) {
                    if (!res.error) {
                        // Fetching Team list successful
                        TLM.printTeamList(res.teams);
                    }
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },
    printTeamList: function (teams) {
        // Method to print team list
        var teamListHtml = '';
        for (var i = 0; i < teams.length; i++) {
            teamListHtml += '<div class="card teams_cards">'; // Team card start
            teamListHtml += '<div class="card-header row" style="margin:0">'; // card header start
            teamListHtml += '<div class="col-xs-6" style="padding-right:5px;">'; // card header detail holder start
            teamListHtml += '<h6 class="label-team-name">Name</h6>'; // team name heading
            teamListHtml += '<h4 class="name-of-team" data-teamid="' + teams[i]._id + '" onclick="TLM.showTeamDetails(this)">' + teams[i].name + '</h4>'; // team name holder
            teamListHtml += '</div>'; // card header detail holder end
            teamListHtml += '<div class="col-xs-6" style="padding-left:3px; padding-right:12px;">'; // team lead holder start
            teamListHtml += '<h6 class="label-team-lead">Team Lead</h6>'; // team lead heading
            if (typeof teams[i]._teamLead != 'undefined')
                teamListHtml += '<h4 class="leader-name">' + teams[i]._teamLead.name.full + '</h4>';
            teamListHtml += '</div>'; // team lead holder end
            teamListHtml += '</div>'; // card header end
            teamListHtml += '<div class="card-block">'; // card detail block start
            teamListHtml += '<div class="team-details">'; // team details block start
            teamListHtml += '<p><span>' + teams[i].memberCount + '</span> Members</p>'; // total team members
            teamListHtml += '<p><span>' + teams[i].projectCount + '</span> Projects</p>'; // total team projects
            if (teams[i].isActive) {
                // Team is active
                teamListHtml += '<button class="btn btn-xs btn-active">Active</button> &nbsp;'; // Active button
                teamListHtml += '<button class="btn btn-xs btn-deactivate" data-teamid="' + teams[i]._id + '" onclick="TLM.changeTeamStatus(this)">Deactivate</button>'; // Deactive button
            } else {
                // Team is inactive
                teamListHtml += '<button class="btn btn-xs btn-active deactivate">Inactive</button> &nbsp;'; // Active button
                teamListHtml += '<button class="btn btn-xs btn-deactivate active" data-teamid="' + teams[i]._id + '" onclick="TLM.changeTeamStatus(this)">Activate</button>'; // Deactive button
            }

            teamListHtml += '</div>'; // team details block end
            teamListHtml += '</div>'; // card detail block end
            teamListHtml += '</div>'; // Team card end
        }
        $("#teamlist").html(teamListHtml); // clear and update the list of teams
    },
    showTeamDetails: function (selectedTeam) {
        // Method to show team details
        localStorage.selectedteam = $(selectedTeam).data('teamid');
        CM.redirectToPage('teamdetails.html');
    },
    changeTeamStatus: function (teamToUpdate) {
        // Method to change the status of the Team
        if (CM.checkConnection()) {
            // Internet available
            var teamID = $(teamToUpdate).data("teamid");
            CM.showLoader(InfoText.changing_team_status);
            $.ajax({
                url: localStorage.wsurl + "team/toggleactive/" + teamID,
                type: 'PUT',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                success: function (res) {
                    if (!res.error) {
                        // Updating Team status successful
                        TLM.fetchTeamList();
                    }
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    }
};

document.addEventListener("deviceready", TLM.fetchTeamList, false);