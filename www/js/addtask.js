/****Add Task page methods(atpm)****/
console.warn("ATPM called");
var ATPM={
    /**Add tasks page literal**/
    validateAddTaskForm:function(){
        // Method to Validate Add Task Form
        if($("#tasksubject").val()==""){
            // If New task subject is empty
            CM.showAlert(ErrorText.new_task_subject_blank);
			$("#tasksubject").focus();
        }else if($("#taskdescription").val()==""){
            // If New Task description is empty
            CM.showAlert(ErrorText.new_task_desc_blank);
			$("#taskdescription").focus();
        }else if($("#taskstartdate").val()==""){
            // If New Task start date is empty
            CM.showAlert(ErrorText.new_task_start_date_blank);
			$("#taskstartdate").focus();
        }else if($("#taskenddate").val()==""){
            // If New Task end date is empty
            CM.showAlert(ErrorText.new_task_end_date_blank);
			$("#taskenddate").focus();
        }else if($("#taskincentivepoints").val()==""){
            // If New Task end date is empty
            CM.showAlert(ErrorText.new_task_incentive_blank);
			$("#taskincentivepoints").focus();
        }else 
        // Everything has been filled save the task
        ATPM.saveTask()
    },saveTask:function(){
        // Method to save new task
         if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.task_saving);
            var newTaskObj = {};
            newTaskObj.newTask={};
            newTaskObj.newTask.name = $("#tasksubject").val();
            newTaskObj.newTask.desc=$("#taskdescription").val();
            newTaskObj.newTask.startDate = moment($("#taskstartdate").val()).format('DD/MM/YYYY HH:mm').toString();
            newTaskObj.newTask.endDate = moment($("#taskenddate").val()).format('DD/MM/YYYY HH:mm').toString();
            newTaskObj.newTask.incentivePoints = $("#taskincentivepoints").val();
            newTaskObj.newTask._projectId = localStorage.selectedprojectid;
            $.ajax({
                url: localStorage.wsurl + "tasks",
                type: 'POST',                
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                data: newTaskObj,
                success: function (res) {
                    if (!res.error) {
                        console.warn(JSON.stringify(res));
                        // Redirect to Projects page after 2s to avoid showing old data
                        CM.redirectToPage('projects.html');                            
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    console.warn(JSON.stringify(err));
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    }
};