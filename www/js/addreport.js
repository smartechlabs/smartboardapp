/****Add Report page methods(arpm)****/
console.warn("ARPM loaded");
var reportSub = $("#reportsubject");
var reportDes = $("#reportdescription");
var reportDate=$("#reportdate");
var ARPM = {
    /**Add Report page literal**/
    validateReportForm: function () {
        // Method to validate Add Report form
        if (reportDate.val() === '') {
            CM.showAlert(ErrorText.report_date_blank);
            reportDate.focus();
        } else if (reportSub.val() === '') {
            CM.showAlert(ErrorText.report_subject_blank);
            reportSub.focus();
        }else if (reportDes.val() === '') {
            CM.showAlert(ErrorText.report_description_blank);
            reportDes.focus();
        } else {
            if (typeof localStorage.selectedReportData !== 'undefined')
                ARPM.updateReport();
            else
                ARPM.saveReport();
        }
    },
    fillReports: function () {
        // Method to prefill Notes fields
        if (typeof localStorage.selectedReportData !== 'undefined') {
            // This is edit so fill data
            var savedReportData = JSON.parse(localStorage.selectedReportData);
            reportSub.val(savedReportData.reportsub);
            reportDes.val(savedReportData.reportdesc);
            reportDate.val(moment(savedReportData.reportdate).format('YYYY-MM-DD'));
        }
    },
    updateReport: function () {
        // Method to Update/Edit Note
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.note_updating);
            var newReportObj = {};
            var savedReportData = JSON.parse(localStorage.selectedReportData);
            newReportObj.reportid = savedReportData.reportid;
            newReportObj.dueDate = moment(reportDate.val()).format('DD-MM-YYYY');
            newReportObj.name = reportSub.val()||'';
            newReportObj.desc = reportDes.val();
            $.ajax({
                url: localStorage.wsurl + "tasks/reports/" + newReportObj.reportid,
                type: 'PUT',
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                data: newReportObj,
                success: function (res) {
                    if (!res.error) {
                        console.warn(JSON.stringify(res));
                        reportSub.val(''); // clear fields
                        reportDes.val('');
                        reportDate.val('');
                        CM.redirectToPage('viewtask.html'); // redirect back to tasks view page
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },
    saveReport: function () {
        // Method to save Note
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.note_saving);
            var newReportObj = {};
            newReportObj.dueDate = moment(reportDate.val()).format('DD-MM-YYYY');
            newReportObj.name = reportSub.val()||'';
            newReportObj.desc = reportDes.val();
            $.ajax({
                url: localStorage.wsurl + "tasks/reports/" + localStorage.selectedtaskid,
                type: 'POST',
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                data: newReportObj,
                success: function (res) {
                    if (!res.error) {
                        console.warn(JSON.stringify(res));
                        reportSub.val(''); // clear fields
                        reportDes.val('');
                        reportDate.val('');
                        CM.redirectToPage('viewtask.html'); // redirect back to tasks view page
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    }
};


document.addEventListener("deviceready", ARPM.fillReports, false);