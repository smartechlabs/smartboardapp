/****Project list page methods(plm)****/
console.warn("PLM loaded");
var PLM = {
    /**Project list page literal**/
    fetchProjectList: function () {
        // Method to fetch team list
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.projectlist_fetching);
            $.ajax({
                url: localStorage.wsurl + "projects",
                type: 'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                success: function (res) {
                    if (!res.error) {
                        // Fetching Project list successful
                        PLM.printProjectList(res.projects, res.teams);
                    }
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);

    },
    printProjectList: function (projects, teams) {

        // Method to print project list
        var projectListHtml = '';
        for (var i = 0; i < projects.length; i++) {
            projectListHtml += '<div>'; // project container start
            projectListHtml += '<div class="row my-projects-collapse" data-toggle="collapse" href="#collapse' + i + '" aria-expanded="false" aria-controls="collapse' + i + '">'; // collapse header start
            projectListHtml += '<div class="col-xs-5" style="padding-left:5px; padding-right:5px;">'; // project name location holder start
            projectListHtml += '<h4 class="event-name">' + projects[i].name + '</h4>'; // event name
            var projectTime = moment(projects[i].dateTime).format('h:MM A, MMM');
            projectListHtml += '<span class="event-venue">' + projectTime + ' at ' + projects[i].location + '</span>'; // time and location
            projectListHtml += '</div>'; // project name location holder
            projectListHtml += '<div class="col-xs-5" style="padding-left:5px; padding-right:5px;">' // incentive holder start
            projectListHtml += '<span class="label-total-incentive">Total Incentive</span>'; // incentive title
            projectListHtml += '<h4 class="event-points">' + projects[i].incentivePoints + ' Points</h4>'; // incentive value
            projectListHtml += '</div>'; // incentive holder end
            projectListHtml += '<div class="col-xs-1" style="padding-left:5px; padding-right:5px;">'; // edit button holder start
            projectListHtml += '<i class="fa fa-edit event-edit-icon" data-projectid=' + projects[i]._id + ' data-projectincentive='+projects[i].incentivePoints+'></i>'; // edit button
            projectListHtml += '</div>'; // edit button holder end
            projectListHtml += '<div class="col-xs-1" style="padding-left:5px; padding-right:5px;">'; // collapse close icon start
            projectListHtml += '<i class="fa fa-arrow-circle-down open-closed-icon"></i>'; // close icon
            projectListHtml += '</div>'; // collapse close icon holder end
            projectListHtml += '</div>'; // collapse header end
            projectListHtml += '<div class="collapse" id="collapse' + i + '">'; // collapse body start
            projectListHtml += PLM.makeTaskListHtml(projects[i]._tasks, projects[i]._teamId.members);
            projectListHtml += '<button type="button" class="btn btn-lg btn-block green-btn" style="margin-top:0" onclick=PLM.showAddTaskPage("' + projects[i]._id + '")>Add Task</button>'; // add task button
            projectListHtml += '</div>'; // collapse body end
            projectListHtml += '</div>'; // project container end
        }
        $("#projectlist").html(projectListHtml); // clear and update the list of projects
        // PLM.printTeamList(teams); // Print Teamlist and append it
        $(".event-edit-icon").on('click', function (e) {
            e.stopPropagation(); // prevent expanding of collapse
            PLM.promptAddIncentive($(this).data("projectid"),$(this).data("projectincentive"));
        });
    },
    promptAddIncentive: function (projectId,projectIncentive) {
        // Method to show dialog to prompt the user to enter incentive points
        navigator.notification.prompt(
            'Please enter Incentive points', // message
            function(results){
                if(results.buttonIndex==1)
                    PLM.updateIncentive(projectId,results.input1);
            }, // callback to invoke
            'Edit Incentive', // title
            ['Update', 'Cancel'], // buttonLabels
            projectIncentive // defaultText
        );
    },updateIncentive:function(projectId,incentivePoint){
        // Method to update total incentive points of a project
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.project_incentive_updating);
            
            $.ajax({
                url: localStorage.wsurl + "projects/incentive/"+projectId,
                type: 'PUT',                
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                data: {newIncentive:incentivePoint},
                success: function (res) {
                    if (!res.error) {
                        console.warn(JSON.stringify(res));
                        PLM.fetchProjectList(); // fetch the list again
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },
    makeTaskListHtml: function (tasks, teams) {
        // Method to make task list html
        var taskListHtml = '';
        if (tasks.length > 0) {
            // Tasks exist
            for (var i = 0; i < tasks.length; i++) {
                var oddOrEvenCard = "";
                if (i % 2 == 0)
                    // Even card
                    oddOrEvenCard = "task_card-even";
                else
                    // Odd card
                    oddOrEvenCard = "task_card-odd";
                taskListHtml += '<div class="card ' + oddOrEvenCard + '">'; // task holder start
                taskListHtml += '<div class="card-block row" style="margin:0">'; // task row start
                taskListHtml += '<div class="col-xs-6" style="padding:0 5px 0 8px;">'; // task name holder start
                taskListHtml += '<p class="label-note" onclick="PLM.showTaskInfoPage(this)" data-tname="' + tasks[i].name + '" data-tid="' + tasks[i]._id + '"><span>' + tasks[i].name + ':</span></p>'; // task name
                taskListHtml += '</div>'; // task name holder end
                taskListHtml += '<div class="col-xs-6" style="padding:0 5px;">'; // task date holder start
                var taskStartDate = moment(tasks[i].startDate).format('D');
                var taskEndDate = moment(tasks[i].endDate).format('D MMM');
                var taskCompleted = "";
                var taskPending = "";
                var taskInProgress = "";

                switch (tasks[i].status) {
                    // switch case to find the Task progress and make it selected in drop down
                    case 'Complete':
                        taskCompleted = "selected";
                        break;
                    case 'Pending':
                        taskPending = "selected";
                        break;
                    case 'In progress':
                        taskInProgress = "selected";
                        break;
                }
                taskListHtml += '<p class="task-dates">Start & End Date: ' + taskStartDate + ' to ' + taskEndDate + '</p>'; // task date
                taskListHtml += '</div>'; // task date holder end
                taskListHtml += '<div class="col-xs-12">'; // task description holder start
                taskListHtml += '<p>' + tasks[i].desc + '</p>'; // task description
                taskListHtml += '</div>'; // task description holder end
                taskListHtml += '<form>'; // task form start
                taskListHtml += '<div class="col-xs-6">'; // assign to holder start
                taskListHtml += '<div class="form-group task-assign-select">'; // assign to inner holder start
                taskListHtml += '<label>Assign to</label>'; // assign to label
                taskListHtml += '<select class="form-control ppassigneemenu" data-tid="' + tasks[i]._id + '" onchange="PLM.changeAssignee(this)">' + PLM.makeTeamListHtml(teams, tasks[i]._assignedTo) + '</select>';
                taskListHtml += '</div>'; // assign to inner holder end 
                taskListHtml += '</div>'; // assign to holder end
                taskListHtml += '<div class="col-xs-6">'; // status holder start
                taskListHtml += '<div class="form-group task-status-select">'; // status inner holder start
                taskListHtml += '<label>Status</label>'; // status label
                taskListHtml += '<select class="form-control" data-tid="' + tasks[i]._id + '" onchange="PLM.updateTaskStatus(this)">'; // status select start
                taskListHtml += '<option value="Select">Select</option>';
                taskListHtml += '<option value="Complete" ' + taskCompleted + '>Complete</option>';
                taskListHtml += '<option value="Pending" ' + taskPending + '>Pending</option>';
                taskListHtml += '<option value="Inprogress" ' + taskInProgress + '>In progress</option>';
                taskListHtml += '</select>'; // status select end
                taskListHtml += '</div>'; // status inner holder end
                taskListHtml += '</div>'; // status holder end
                taskListHtml += '</form>'; // task form end
                taskListHtml += '<div>'; // task expenses parent holder start
                taskListHtml += '<div class="col-xs-6">'; // task expense column start
                taskListHtml += '<div class="expenses">'; // task expense holder start
                taskListHtml += '<label>Expenses</label>'; // expense label
                taskListHtml += '<span>&#8377 ' + CM.replaceNull(tasks[i]._expenses) || 0 + '<i class="fa fa-info-circle"></i></span>'; // expense
                taskListHtml += '</div>'; // task expense holder end
                taskListHtml += '</div>'; // task expense column end
                taskListHtml += '<div class="col-xs-6">'; // task incentive column start
                taskListHtml += '<div class="incentives">'; // task incentive holder start
                taskListHtml += '<label>Incentive</label>'; // incentive label
                taskListHtml += '<span>' + tasks[i].incentivePoints + '</span>'; // incentive value
                taskListHtml += '</div>'; // task incentive holder end
                taskListHtml += '</div>'; // task incentive column end
                taskListHtml += '</div>'; // task expenses parent holder end
                taskListHtml += '</div>'; // task row end
                taskListHtml += '</div>'; // task holder end
            }
            return taskListHtml;

        } else {
            // No tasks assigned/pending in the project
            return taskListHtml;
        }
    },
    makeTeamListHtml: function (teams, assignedId) {
        // Method to print team list
        var teamListOptions = '<option value="select">Select</option>';
        for (var i = 0; i < teams.length; i++) {
            var selectAssignee = "";
            if (teams[i]._studentId.id === assignedId)
                selectAssignee = "selected";
            teamListOptions += '<option value="' + teams[i]._studentId.id + '" ' + selectAssignee + '>' + teams[i]._studentId.name.full + '</option>';
        }
        // $(".ppassigneemenu").html(teamListOptions);
        return teamListOptions;
    },
    showTaskInfoPage: function (selectedTask) {
        // Method to get the selected task
        localStorage.selectedtaskid = $(selectedTask).data('tid');
        CM.redirectToPage('viewtask.html');
    },
    showAddTaskPage: function (selectedProject) {
        // Method to show the selected project
        localStorage.selectedprojectid = selectedProject;
        CM.redirectToPage('addtask.html');
    },
    updateTaskStatus: function (selectedTask) {
        // Method to update status of a task
        if ($(selectedTask).val() !== "Select") {
            var taskStatus = $(selectedTask).val() == "Inprogress" ? "In progress" : $(selectedTask).val();
            if (CM.checkConnection()) {
                // Internet available
                CM.showLoader(InfoText.project_task_status_updating);
                $.ajax({
                    url: localStorage.wsurl + "tasks/status/" + $(selectedTask).data("tid") + "/" + taskStatus,
                    type: 'PUT',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                    },
                    success: function (res) {
                        if (!res.error) {
                            console.warn(JSON.stringify(res));
                            // Todo: Show toast alert to say success message
                            PLM.fetchProjectList();
                        } else
                            CM.showAlert(res.message);
                        waitingDialog.hide();
                    },
                    error: function (err) {
                        waitingDialog.hide();
                    }
                });
            } else CM.showAlert(ErrorText.no_internet);
        }
    },
    changeAssignee: function (selectedAssignee) {
        // Method to update status of a task
        if ($(selectedAssignee).val() !== "Select") {
            if (CM.checkConnection()) {
                // Internet available
                CM.showLoader(InfoText.project_task_assignee_updating);
                $.ajax({
                    url: localStorage.wsurl + "tasks/assign/" + $(selectedAssignee).data("tid") + "/" + $(selectedAssignee).val(),
                    type: 'PUT',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                    },
                    success: function (res) {
                        if (!res.error) {
                            console.warn(JSON.stringify(res));
                            // Todo: Show toast alert to say success message
                            PLM.fetchProjectList();
                        } else
                            CM.showAlert(res.message);
                        waitingDialog.hide();
                    },
                    error: function (err) {
                        waitingDialog.hide();
                    }
                });
            } else CM.showAlert(ErrorText.no_internet);
        }
    }
};

document.addEventListener("deviceready", PLM.fetchProjectList, false);