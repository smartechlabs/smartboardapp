/****Add Goals page methods(agpm)****/
console.warn("AGPM called");
var AGPM = {
    /**Add goals page literal**/
    saveGoal: function () {
        // Method to save goal to DB
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.goal_saving);
            var newGoalObj = {};
            newGoalObj.newGoal = {};
            newGoalObj.newGoal.name = $("#goalname").val();
            newGoalObj.newGoal.dueDate = moment($("#goalduedate").val()).format('DD/MM/YYYY').toString();
            newGoalObj.newGoal.weight = $("#goalweight").val();
            newGoalObj.newGoal.completionPercentage = $("#goalpercentprogress").val();
            console.log(newGoalObj.newGoal);
            $.ajax({
                url: localStorage.wsurl + "profile/goal",
                type: 'POST',
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                data: newGoalObj,
                success: function (res) {
                    if (!res.error) {
                        console.warn(JSON.stringify(res));
                        MPPM.fetchMyProfileDetails();
                        CM.redirectToPage('mygoals.html');
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);

    }, validateGoalForm: function () {
        // Method to validate add goal form
        if ($("#goalname").val() == "") {
            // If New goal name is empty
            CM.showAlert(ErrorText.new_goal_name_blank);
            $("#goalname").focus();
        } else if ($("#goalduedate").val() == "") {
            // If New goal due date is empty
            CM.showAlert(ErrorText.new_goal_due_date_blank);
            $("#goalduedate").focus();
        } else if ($("#goalweight").val() == "") {
            // If New goal weight is empty
            CM.showAlert(ErrorText.new_goal_weight_blank);
            $("#goalweight").focus();
        } else if ($("#goalpercentprogress").val() == "") {
            // If New goal weight is empty
            CM.showAlert(ErrorText.new_goal_percentage_blank);
            $("#goalpercentprogress").focus();
        } else {
            // Everything filled 
            if (localStorage.goaleditting == "false")
                // Add Goal option choosed so save the goal
                AGPM.saveGoal();
            else if (localStorage.goaleditting == "true")
                // Edit Goal option choosed so update the goal
                AGPM.updatGoal();
        }
    }, checkForEditGoal: function () {
        // Method to check whether Edit Goal or Add Goal
        if (localStorage.goaleditting == "true") {
            // Edit goal option
            // Prefill the saved data
            var savedGoal = JSON.parse(localStorage.savedgoaltoedit);
            $("#goalname").val(savedGoal.goalName);
            $("#goalduedate").val(savedGoal.goalDue);
            $("#goalweight").val(savedGoal.goalWeight);
            $("#goalpercentprogress").val(savedGoal.goalPercent);
            $("#goalpageheading").html("Edit Goal");
        } else console.warn("Add Goal option");
    }, updatGoal: function () {
        // Method to update Goal
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.goal_updating);
            var goalToEdit = JSON.parse(localStorage.savedgoaltoedit);
            var newGoalObj = {};
            newGoalObj.newGoal = {};
            newGoalObj.newGoal.name = $("#goalname").val();
            newGoalObj.newGoal.dueDate = moment($("#goalduedate").val()).format('DD/MM/YYYY HH:mm').toString();
            newGoalObj.newGoal.weight = $("#goalweight").val();
            newGoalObj.newGoal.completionPercentage = $("#goalpercentprogress").val();
            newGoalObj.goalid = goalToEdit.goalId;
            $.ajax({
                url: localStorage.wsurl + "profile/goal/" + goalToEdit.goalId,
                type: 'PUT',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                data: newGoalObj,
                success: function (res) {
                    if (!res.error) {
                        console.warn(JSON.stringify(res));
                        MPPM.fetchMyProfileDetails();
                        // Redirect to MyGoals page after 2s to avoid showing old data
                        setTimeout(function () {
                            CM.redirectToPage('mygoals.html');
                        }, 2000);
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    }
};
document.addEventListener("deviceready", AGPM.checkForEditGoal, false);