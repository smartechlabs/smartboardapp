/****Login page methods(lpm)****/
console.warn("LPM Loaded");
var LPM = {
    /**Login Page method literal**/
    validateLoginForm: function () {
        // Method to validate Login Form
        if ($("#lemail").val() == "") {
            // If email field is empty
            CM.showAlert(ErrorText.email_blank);
            $("#lemail").focus();
        } else if (CM.isValidEmail($("#lemail").val())) {
            // If email entered is not Valid
            CM.showAlert(ErrorText.new_member_email_invalid);
            $("#lemail").focus();
        } else if ($("#lpass").val() == "") {
            // If password field is empty
            CM.showAlert(ErrorText.login_password_blank);
            $("#lpass").focus();
        } else {
            // All the needed fields are filled try to login the user
            this.loginUser();
        }
    },
    loginUser: function () {
        // Method to login the user
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.login_verification);
            $.ajax({
                url: localStorage.wsurl + "login",
                type: 'POST',
                data: {
                    "loginEmail": $("#lemail").val(),
                    "loginPassword": $("#lpass").val()
                },
                success: function (res) {
                    if (!res.error) {
                        // Login Success save access token
                        localStorage.accesstoken = res.token;
                        CM.redirectToPage('teamlist.html');
                        // CM.redirectToPage('projects.html');
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },
    onDeviceReady: function(){
        if (localStorage.getItem('accesstoken')) {
            CM.redirectToPage('teamlist.html');
        }
        document.addEventListener("backbutton", function(){
            navigator.app.exitApp();
        }, false);
    }
};
document.addEventListener("deviceready", LPM.onDeviceReady, false);