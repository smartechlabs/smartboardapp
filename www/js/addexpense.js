/****Add Expense page methods(aepm)****/
console.warn("AEPM called");
var AEPM = {
    /**Add expense page literal**/
    validateAddExpenseForm: function () {
        // Method to Validate Add Expense Form
        if ($("#expensedescription").val() == "") {
            // If New expense description is empty
            CM.showAlert(ErrorText.new_expense_description_blank);
            $("#expensedescription").focus();
        } else if ($("#expenseamount").val() == "") {
            // If New expense amount is empty
            CM.showAlert(ErrorText.new_expense_amount_blank);
            $("#expenseamount").focus();
        } else
            // Everything has been filled save the expense
            AEPM.saveExpense()
    },
    saveExpense: function () {
        // Method to save new task
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.expense_saving);

            $.ajax({
                url: localStorage.wsurl + "tasks/expenses/" + localStorage.selectedtaskid,
                type: 'POST',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                data: {
                    amount: $("#expenseamount").val()
                },
                crossDomain: true,
                success: function (res) {
                    console.warn("save Expense:", JSON.stringify(res));
                    if (!res.error) {
                        console.warn(JSON.stringify(res));
                        CM.redirectToPage('projects.html');
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    }
};