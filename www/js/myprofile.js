/****My Profile page methods(mppm)****/
console.warn("MPPM loaded");

var MPPM = {
    /**My profile page literal**/
    fetchMyProfileDetails: function() {
        // Method to fetch the current user details
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.myprofile_fetching);
            $.ajax({
                url: localStorage.wsurl + "profile",
                type: 'GET',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                success: function(res) {
                    if (!res.error) {
                        // Fetching user profile successful
                        localStorage.myprofile=JSON.stringify(res);
                        MPPM.printUserProfile(res);
                    }
                    waitingDialog.hide();
                },
                error: function(err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    },printUserProfile:function(profileInfo){
    	// Method to print or fill user details
    	$("#profile_title").html(profileInfo.title); // fill profile title
    	$("#profile_fullname").html(profileInfo.currentUser.name.full); // fill profile full name
    	$("#profile_email").html(profileInfo.currentUser.email); // fill profile email
        $("#profile_address").html(profileInfo.currentUser.address || 'N/A'); // fill profile address
        $("#profile_incentive").html(profileInfo.currentUser.myTotalIncentive); // fill profile incentive
    }
};

document.addEventListener("deviceready", MPPM.fetchMyProfileDetails, false);