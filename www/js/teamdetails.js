/****Team details page methods(tdpm)****/
console.warn("TDPM loaded");
var TDPM = {
    /**Team details page literal**/
    fetchTeamDetails: function () {
        // Method to get Team details
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.team_detail_fetching);
            $.ajax({
                url: localStorage.wsurl + "team/" + localStorage.selectedteam,
                type: 'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                success: function (res) {
                    if (!res.error) {
                        // Fetching Team details successful
                        TDPM.printTeamDetails(res);
                    }
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    }, printTeamDetails: function (teamInfo) {
        // Method to print team info
        console.warn(teamInfo);
        $("#tdname").html(teamInfo.team.name); // fill team name
        $("#tdmemberslist").html(TDPM.frameMembersListHtml(teamInfo.team.members, teamInfo.designations));
        $("#tdprojectslist").html(TDPM.frameProjectsListHtml(teamInfo.projects, teamInfo.team.members));
        $(".event-edit-icon").on('click', function (e) {
            e.stopPropagation(); // prevent expanding of collapse
            TDPM.promptAddIncentive($(this).data("projectid"), $(this).data("projectincentive"));
        });
    },
    promptAddIncentive: function (projectId, projectIncentive) {
        // Method to show dialog to prompt the user to enter incentive points
        navigator.notification.prompt(
            'Please enter Incentive points', // message
            function (results) {
                if (results.buttonIndex == 1)
                    TDPM.updateIncentive(projectId, results.input1);
            }, // callback to invoke
            'Edit Incentive', // title
            ['Update', 'Cancel'], // buttonLabels
            projectIncentive // defaultText
        );
    }, getMembersList: function () {
        // Method to get the list of members
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.memberlist_fetching);
            $.ajax({
                url: localStorage.wsurl + "availableforteam",
                type: 'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                success: function (res) {
                    if (!res.error) {
                        // Fetching Award list successful
                        TDPM.printMembersList(res.availableStudents);
                    }
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    }, printMembersList: function (members) {
        // Method to print the list of members got
        var membersListHtml = '<option value="">Select</option>';
        for (var i = 0; i < members.length; i++) {
            var member = members[i];
            membersListHtml += '<option value=' + member._id + '>' + member.name.full + '</option>';
        }
        $('#availablememberslist').html(membersListHtml);
    },
    updateIncentive: function (projectId, incentivePoint) {
        // Method to update total incentive points of a project
        if (CM.checkConnection()) {
            // Internet available
            CM.showLoader(InfoText.project_incentive_updating);

            $.ajax({
                url: localStorage.wsurl + "projects/incentive/" + projectId,
                type: 'PUT',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                data: { newIncentive: incentivePoint },
                success: function (res) {
                    if (!res.error) {
                        console.warn(JSON.stringify(res));
                        TDPM.fetchTeamDetails(); // fetch the list again
                    } else
                        CM.showAlert(res.message);
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    }, frameMembersListHtml: function (teamMembers, designations) {
        // Method to frame members list html
        var membersHtml = '';
        for (var i = 0; i < teamMembers.length; i++) {
            var tm = teamMembers[i]._studentId;
            var currentStatus = 'Activate';
            var statusToChange = 'Inactive';
            membersHtml += '<div class="card team-info_cards">'; // member card start
            membersHtml += '<div class="card-header row" style="margin:0">'; // member card header start
            membersHtml += '<div class="col-xs-12">'; // member name holder start
            membersHtml += '<h4 class="member-name">' + tm.name.full + '</h4>'; // member name
            membersHtml += '</div>'; // member name holder end
            membersHtml += '</div>'; // member card header end
            membersHtml += '<div class="card-block row" style="margin:0">'; // member info block start
            membersHtml += '<p class="card-blockquote"><i class="fa fa-envelope"></i> ' + tm.email + '</p>'; // member email id
            membersHtml += '<div class="col-xs-7" style="padding-left:0">'; // member designation list holder start
            /* membersHtml += '<a class="btn btn-default btn-select">'; // member button start
            membersHtml += '<input type="hidden" class="btn-select-input" id="" name="" value="" />'; // dummy input
            membersHtml += '<span class="btn-select-value">Select</span>'; // select label
            membersHtml += '<span class="btn-select-arrow fa fa-angle-down"></span>'; // dropdown icon
            membersHtml += '<ul>'; // designation holder start
            membersHtml += TDPM.frameDesignationsListHtml(designations); // designation list
            membersHtml += '</ul>'; // designation holder end
            membersHtml += '</a>'; // member button end */
            membersHtml += '<div class="form-group select-member-role-wrap">'; // designtion holder start
            membersHtml += '<select class="form-control select-member-role" data-memberid="' + tm._id + '" onchange="TDPM.changeMemberDesignation(this)">'; // designation select menu start
            membersHtml += TDPM.frameDesignationsListHtml(designations, teamMembers[i].designation); // designation list
            membersHtml += '</select>'; // designation select menu end
            membersHtml += '<i class="fa fa-angle-down"></i>'; // designation drop down
            membersHtml += '</div>'; // designation holder end
            membersHtml += '</div>'; // member designation list holder end
            membersHtml += '<div class="col-xs-4 text-center" style="padding:0 5px;">'; // deactivate button holder start
            if (teamMembers[i].status === 'Inactive') {
                currentStatus = 'Activate';
                statusToChange = 'Active';
            }
            else {
                currentStatus = 'Deactivate';
                statusToChange = 'Inactive';
            }
            membersHtml += '<button class="btn btn-xs btn-deactivate" data-memberid=' + tm._id + ' data-statustochange=' + statusToChange + ' onclick="TDPM.updateMemberStatus(this)">' + currentStatus + '</button>'; // deactivate button
            membersHtml += '</div>'; // deactivate button holder end
            membersHtml += '</div>'; // member info block end
            membersHtml += '</div>'; // member card end
        }
        return membersHtml;
    }, frameDesignationsListHtml: function (designations, selectedDesignation) {
        // Method to print the designation list
        var designationHtml = '';
        for (var i = 0; i < designations.length; i++) {
            var designation = designations[i];
            if (designation.name === selectedDesignation)
                designationHtml += '<option selected value="' + designation.name + '" data-level="' + designation.level + '">' + designation.name + '</li>';
            else
                designationHtml += '<option value="' + designation.name + '" data-level="' + designation.level + '">' + designation.name + '</li>';
        }
        return designationHtml;

    }, updateMemberStatus: function (selectedMember) {
        // Method to update the status of the Member
        if (CM.checkConnection()) {
            // Internet available
            var selectedMember = $(selectedMember);
            var memberId = selectedMember.data("memberid");
            var statusToChange = selectedMember.data("statustochange");
            CM.showLoader(InfoText.changing_member_status);
            $.ajax({
                url: localStorage.wsurl + "/teammember/status/" + localStorage.selectedteam + "/" + memberId + "/" + statusToChange,
                type: 'PUT',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                },
                success: function (res) {
                    if (!res.error) {
                        // Updating Member status successful
                        TDPM.fetchTeamDetails();
                    }
                    waitingDialog.hide();
                },
                error: function (err) {
                    waitingDialog.hide();
                }
            });
        } else CM.showAlert(ErrorText.no_internet);
    }, changeMemberDesignation: function (selectedMember) {
        // Method to update status of a task
        var selectedMember = $(selectedMember);
        if (selectedMember.val() !== "Select") {
            if (CM.checkConnection()) {
                // Internet available
                var selectedDesignation = selectedMember.val();
                var memberId = selectedMember.data('memberid');
                var selectedDesignationLevel = selectedMember.find(':selected').data('level');
                var designationData = {
                    'designation': selectedDesignation,
                    'level': selectedDesignationLevel
                };
                CM.showLoader(InfoText.team_member_designation_updating);
                $.ajax({
                    url: localStorage.wsurl + "teammember/designation/" + localStorage.selectedteam + "/" + memberId,
                    type: 'PUT',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                    },
                    data: designationData,
                    success: function (res) {
                        if (!res.error) {
                            TDPM.fetchTeamDetails();
                        } else
                            CM.showAlert(res.message);
                        waitingDialog.hide();
                    },
                    error: function (err) {
                        waitingDialog.hide();
                    }
                });
            } else CM.showAlert(ErrorText.no_internet);
        }
    }, frameProjectsListHtml: function (projects, members) {
        // Method to frame project lists
        var projectListHtml = '';
        for (var i = 0; i < projects.length; i++) {
            var project = projects[i];
            projectListHtml += '<div class="row my-projects-collapse" data-toggle="collapse" href="#' + project._id + '" aria-expanded="false" aria-controls="' + project._id + '">'; // project collapsible header start
            projectListHtml += '<div class="col-xs-5" style="padding:0 5px;">'; // project name holder start
            projectListHtml += '<h4 class="event-name">' + project.name + ' </h4>'; // project name
            projectListHtml += '<span class="event-venue">' + moment(project.dateTime).format('h:mm A, Do MMM') + ' at ' + project.location + '</span>'; // project location and time
            projectListHtml += '</div>'; // project name holder end
            projectListHtml += '<div class="col-xs-5" style="padding:0 5px;">'; // incentive holder start
            projectListHtml += '<span class="label-total-incentive">Total Incentive</span>'; // incentive label
            projectListHtml += '<h4 class="event-points">' + project.incentivePoints + ' Points</h4>'; // incentive points
            projectListHtml += '</div>'; // incentive holder end
            projectListHtml += '<div class="col-xs-1" style="padding:0 5px;">'; // incentive edit button holder start
            projectListHtml += '<i class="fa fa-edit event-edit-icon" data-projectid=' + project._id + ' data-projectincentive=' + project.incentivePoints + '></i>'; // edit button
            projectListHtml += '</div>'; // incentive edit button holder end
            projectListHtml += '<div class="col-xs-1" style="padding:0 5px;">'; // collapsible icon holder start
            projectListHtml += '<i class="fa fa-arrow-circle-down open-closed-icon"></i>'; // collapsible icon
            projectListHtml += '</div>'; // collapsible icon holder end
            projectListHtml += '</div>'; // project collapsible header end

            projectListHtml += '<div class="collapse" id="' + project._id + '">'; // project collapsible body start
            projectListHtml += TDPM.frameTasksListHtml(project._tasks, members);
            projectListHtml += '<button type="button" class="btn btn-lg btn-block green-btn" style="margin-top:0" onclick=TDPM.showAddTaskPage("' + project._id + '")>Add Task</button>';
            projectListHtml += '</div>'; // project collapsible body end
        }
        return projectListHtml;
    }, frameTasksListHtml: function (tasks, members) {
        // Method to frame Tasks list html
        var tasksHtml = '';
        for (var i = 0; i < tasks.length; i++) {
            var task = tasks[i];
            var oddOrEvenTask = 'task_card-odd';
            if (i % 2 == 0)
                oddOrEvenTask = 'task_card-even';
            var taskCompleted = "";
            var taskPending = "";
            var taskInProgress = "";

            switch (task.status) {
                // switch case to find the Task progress and make it selected in drop down
                case 'Complete':
                    taskCompleted = "selected";
                    break;
                case 'Pending':
                    taskPending = "selected";
                    break;
                case 'In progress':
                    taskInProgress = "selected";
                    break;
            }
            tasksHtml += '<div class="card ' + oddOrEvenTask + '">'; // task holder start
            tasksHtml += '<div class="card-block row" style="margin:0">'; // task sub holder start
            tasksHtml += '<div class="col-xs-6" style="padding:0 5px 0 8px;">'; // task label holder start
            tasksHtml += '<p class="label-note"><span class="black-text">Task:</span></p>'; // task label
            tasksHtml += '</div>'; // task label holder end
            tasksHtml += '<div class="col-xs-6" style="padding:0 5px;">'; // task date holder start
            tasksHtml += '<p class="task-dates"><b>Start & End Date:</b> ' + moment(task.startDate).format('D') + ' to ' + moment(task.endDate).format('D') + ' ' + moment(task.endDate).format('MMM') + '</p>'; // task date
            tasksHtml += '</div>'; // task date holder end
            tasksHtml += '<div class="col-xs-12">'; // task title holder start
            tasksHtml += '<h4 class="task-title" onclick="TDPM.showTaskInfoPage(this)" data-tname="' + task.name + '" data-tid="' + task._id + '">' + task.name + '</h4>'; // task title
            tasksHtml += '<p>' + task.desc + '</p>'; // task description
            tasksHtml += '</div>'; // task title holder end
            tasksHtml += '<form>'; // task form start
            tasksHtml += '<div class="col-xs-6">'; // assign to holder start
            tasksHtml += '<div class="form-group task-assign-select">'; // assign to inner holder start
            tasksHtml += '<label>Assign to</label>'; // assign to label
            tasksHtml += '<select class="form-control ppassigneemenu" data-tid="' + task._id + '" onchange="TDPM.changeAssignee(this)">' + TDPM.frameMemberListHtml(members, task._assignedTo) + '</select>';
            tasksHtml += '</div>'; // assign to inner holder end 
            tasksHtml += '</div>'; // assign to holder end
            tasksHtml += '<div class="col-xs-6">'; // status holder start
            tasksHtml += '<div class="form-group task-status-select">'; // status inner holder start
            tasksHtml += '<label>Status</label>'; // status label
            tasksHtml += '<select class="form-control" data-tid="' + task._id + '" onchange="TDPM.updateTaskStatus(this)">'; // status select start
            tasksHtml += '<option value="Select">Select</option>';
            tasksHtml += '<option value="Complete" ' + taskCompleted + '>Complete</option>';
            tasksHtml += '<option value="Pending" ' + taskPending + '>Pending</option>';
            tasksHtml += '<option value="Inprogress" ' + taskInProgress + '>In progress</option>';
            tasksHtml += '</select>'; // status select end
            tasksHtml += '</div>'; // status inner holder end
            tasksHtml += '</div>'; // status holder end
            tasksHtml += '</form>'; // task form end
            tasksHtml += '<div>'; // task expenses parent holder start
            tasksHtml += '<div class="col-xs-6">'; // task expense column start
            tasksHtml += '<div class="expenses">'; // task expense holder start
            tasksHtml += '<label>Expenses</label>'; // expense label
            tasksHtml += '<span>&#8377 ' + CM.replaceNull(task._expenses) || 0 + '<i class="fa fa-info-circle"></i></span>'; // expense
            tasksHtml += '</div>'; // task expense holder end
            tasksHtml += '</div>'; // task expense column end
            tasksHtml += '<div class="col-xs-6">'; // task incentive column start
            tasksHtml += '<div class="incentives">'; // task incentive holder start
            tasksHtml += '<label>Incentive</label>'; // incentive label
            tasksHtml += '<span>' + task.incentivePoints + '</span>'; // incentive value
            tasksHtml += '</div>'; // task incentive holder end
            tasksHtml += '</div>'; // task incentive column end
            tasksHtml += '</div>'; // task expenses parent holder end
            tasksHtml += '</div>'; // task sub holder end
            tasksHtml += '</div>'; // task holder end
        }
        return tasksHtml;
    }, frameMemberListHtml: function (teams, assignedId) {
        // Method to print team list
        var teamListOptions = '<option value="select">Select</option>';
        for (var i = 0; i < teams.length; i++) {
            var selectAssignee = "";
            if (teams[i]._studentId.id === assignedId)
                selectAssignee = "selected";
            teamListOptions += '<option value="' + teams[i]._studentId.id + '" ' + selectAssignee + '>' + teams[i]._studentId.name.full + '</option>';
        }
        // $(".ppassigneemenu").html(teamListOptions);
        return teamListOptions;
    }, changeAssignee: function (selectedAssignee) {
        // Method to update status of a task
        if ($(selectedAssignee).val() !== "Select") {
            if (CM.checkConnection()) {
                // Internet available
                CM.showLoader(InfoText.project_task_assignee_updating);
                $.ajax({
                    url: localStorage.wsurl + "tasks/assign/" + $(selectedAssignee).data("tid") + "/" + $(selectedAssignee).val(),
                    type: 'PUT',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                    },
                    success: function (res) {
                        if (!res.error) {
                            console.warn(JSON.stringify(res));
                            TDPM.fetchTeamDetails();
                        } else
                            CM.showAlert(res.message);
                        waitingDialog.hide();
                    },
                    error: function (err) {
                        waitingDialog.hide();
                    }
                });
            } else CM.showAlert(ErrorText.no_internet);
        }
    }, updateTaskStatus: function (selectedTask) {
        // Method to update status of a task
        if ($(selectedTask).val() !== "Select") {
            var taskStatus = $(selectedTask).val() == "Inprogress" ? "In progress" : $(selectedTask).val();
            if (CM.checkConnection()) {
                // Internet available
                CM.showLoader(InfoText.project_task_status_updating);
                $.ajax({
                    url: localStorage.wsurl + "tasks/status/" + $(selectedTask).data("tid") + "/" + taskStatus,
                    type: 'PUT',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.accesstoken);
                    },
                    success: function (res) {
                        if (!res.error) {
                            TDPM.fetchTeamDetails();
                        } else
                            CM.showAlert(res.message);
                        waitingDialog.hide();
                    },
                    error: function (err) {
                        waitingDialog.hide();
                    }
                });
            } else CM.showAlert(ErrorText.no_internet);
        }
    }, showAddTaskPage: function (selectedProject) {
        // Method to show the selected project
        localStorage.selectedprojectid = selectedProject;
        CM.redirectToPage('addtask.html');
    }, showTaskInfoPage: function (selectedTask) {
        // Method to get the selected task
        localStorage.selectedtaskid = $(selectedTask).data('tid');
        CM.redirectToPage('viewtask.html');
    }

};

document.addEventListener("deviceready", TDPM.fetchTeamDetails, false);